import { TestBed, async } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { AppComponent } from './app.component';
import { HomeComponent } from './dashboard/home/home.component';
import { GruposInvestigacionComponent } from './dashboard/grupos-investigacion/grupos-investigacion.component';
import { ActivatedRoute, RouterModule, Routes } from '@angular/router';

xdescribe('AppComponent', () => {

  const routes: Routes = [
    {
      path: '',
      redirectTo: '/home',
      pathMatch: 'full'
    },
    {
      path: 'home',
      loadChildren: () => import('./dashboard/home/home.module').then(m => m.HomeModule)
    },
    {
      path: 'grupos-investigacion',
      loadChildren: () => import('./dashboard/grupos-investigacion/grupos-investigacion.module').then(m => m.GruposInvestigacionModule)
    },
    {
      path: 'asignar-grupos',
      loadChildren: () => import('./dashboard/asignar-grupos/asignar-grupos.module').then(m => m.AsignarGruposModule)
    },
    {
      path: 'ingestigadores',
      loadChildren: () => import('./dashboard/investigadores/investigadores.module').then(m => m.InvestigadoresModule)
    },
    {
      path: 'produccion',
      loadChildren: () => import('./dashboard/produccion/produccion.module').then(m => m.ProduccionModule)
    },
    {
      path: 'semilleros',
      loadChildren: () => import('./dashboard/semilleros/semilleros.module').then(m => m.SemillerosModule)
    }
  ];

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterModule.forRoot([]),
        RouterTestingModule.withRoutes(
          routes
        )
      ],
      declarations: [
        AppComponent
      ],
    }).compileComponents();
  }));

  it('should create the app', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.componentInstance;
    expect(app).toBeTruthy();
  });

  it(`should have as title 'dashboard-onu'`, () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.componentInstance;
    expect(app.title).toEqual('dashboard-onu');
  });

  it('should render title', () => {
    const fixture = TestBed.createComponent(AppComponent);
    fixture.detectChanges();
    const compiled = fixture.nativeElement;
    //expect(compiled.querySelector('.content span').textContent).toContain('dashboard-onu app is running!');
  });
});
