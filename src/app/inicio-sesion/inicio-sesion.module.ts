import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { InicioSesionRoutingModule } from './inicio-sesion-routing.module';
import { InicioSesionComponent } from './inicio-sesion.component';
import { ReactiveFormsModule } from '@angular/forms';
import { GlobalComponent } from '../dashboard/global-components/global-components';


@NgModule({
  declarations: [InicioSesionComponent],
  imports: [
    CommonModule,
    InicioSesionRoutingModule,
    ReactiveFormsModule,
    GlobalComponent
  ]
})
export class InicioSesionModule { }
