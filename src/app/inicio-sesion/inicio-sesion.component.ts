import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { UserServices } from '../core/services/user.service';
import * as crypto from '../../../node_modules/crypto-js'; 
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { ModalComponent } from '../core/modal/modal.component';
import { PerfilUsuario } from '../core/models/endpoints'

@Component({
  selector: 'app-inicio-sesion',
  templateUrl: './inicio-sesion.component.html',
  styleUrls: ['./inicio-sesion.component.css'],
  providers: [UserServices]
})
export class InicioSesionComponent implements OnInit {
  bsModalRef: BsModalRef;
  userForm: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private userServices: UserServices,
    private modalService: BsModalService
  ) {
    this.userForm = this.formBuilder.group({
      correoElectronico: new FormControl(
        // Valor por defecto
        '',
        // Conjunto de validaciones
        Validators.compose([
          Validators.required,
          Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')
        ])
      ),
      password: new FormControl(
        // Valor por defecto
        '',
        // Conjunto de validaciones
        Validators.compose([
          Validators.required,
          Validators.minLength(5)
        ])
      ),
    });
   }

  ngOnInit(): void {
  }

  submitFormuser(dataUser) {
    localStorage.setItem('datos-usuario',JSON.stringify(PerfilUsuario));
    this.router.navigateByUrl('');
    this.obtenerDataGlobal();

    /*
    dataUser.password = crypto.SHA512(dataUser.password).toString();
    this.userServices.iniciarSesion(JSON.stringify(dataUser)).subscribe(
      response => {
        console.log("response submitFormuser => ", response);

        if (response.codigo == 1) {
          localStorage.setItem('datos-usuario', JSON.stringify(response.objetoRespuestaInvestigador));
          this.router.navigateByUrl('');
          this.obtenerDataGlobal();
        } else {
          this.openModalWithComponent('Error al iniciar sesión', response.descripcion);
        }
      },
      error => {
        this.openModalWithComponent('Error al iniciar sesión', error.message);
        console.log("error submitFormuser => ", error);
      }
    );
    */
  }

  goToComponent(componente){
    this.router.navigate([componente]);
  }

  obtenerDataGlobal() {
    this.userServices.obtenerDataGlobal().subscribe(
      response => {
        console.log("response obtenerDataGlobal => ", response);
        if (response.codigo == 1) {
          localStorage.setItem('AreaConocimiento', JSON.stringify(response.objetoRespuestaArrayAreaConocimiento));
          localStorage.setItem('Ciudades', JSON.stringify(response.objetoRespuestaArrayCiudades));
          localStorage.setItem('Instituciones', JSON.stringify(response.objetoRespuestaArrayInstituciones));
          localStorage.setItem('LineasInvestigacion', JSON.stringify(response.objetoRespuestaArrayLineaInvestigacion));
          localStorage.setItem('TipoPrograma', JSON.stringify(response.objetoRespuestaArrayTipoPrograma));
        }
      },
      error => {
        console.log("error obtenerDataGlobal => ", error);
      }
    );
  }

  openModalWithComponent(titulo, mensaje) {
    const initialState = {
      title: `${titulo}`,
      list: `${mensaje}`,
    };
    this.bsModalRef = this.modalService.show(ModalComponent, { initialState });
    this.bsModalRef.content.closeBtnName = 'Cerrar';
  }

}
