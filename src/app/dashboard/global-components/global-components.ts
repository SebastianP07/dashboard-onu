import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from "@angular/core";
import { CommonModule } from "@angular/common";

import { HeaderComponent } from "./header/header.component";
import { FooterComponent } from "./footer/footer.component";
import { MenuComponent } from "./menu/menu.component";

// ! ngx-bootstrap
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { ModalModule } from 'ngx-bootstrap/modal';
import { SortableModule } from 'ngx-bootstrap/sortable';

// ! Formulario reactivo
import { FormsModule, ReactiveFormsModule } from "@angular/forms";

// ! Data table
import { DataTablesModule } from 'angular-datatables';

// ! NgSelect
import { NgSelectModule } from '@ng-select/ng-select';

//! ngx-sortable
import { SortablejsModule } from 'ngx-sortablejs';



/**
 * TODO: Documentación 
 * * datatables: https://l-lin.github.io/angular-datatables/#/getting-started
 * * NgSelect: https://www.freakyjolly.com/angular-ngselect-with-single-multiple-and-search-filter-tutorial/#.XuF8nLySkdU
 * * ngx-sortable: https://github.com/SortableJS/ngx-sortablejs
 */

@NgModule({
    declarations: [
        HeaderComponent,
        FooterComponent,
        MenuComponent
    ],
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        BsDatepickerModule.forRoot(),
        BsDropdownModule.forRoot(),
        TooltipModule.forRoot(),
        ModalModule.forRoot(),
        DataTablesModule,
        SortableModule.forRoot(),
        NgSelectModule,
        SortablejsModule.forRoot({ animation: 150 })
        ],
    exports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        HeaderComponent,
        FooterComponent,
        MenuComponent,
        BsDropdownModule,
        ModalModule,
        DataTablesModule,
        SortableModule,
        NgSelectModule,
        SortablejsModule
    ],
    schemas: [
        CUSTOM_ELEMENTS_SCHEMA
    ]
})

export class GlobalComponent { }