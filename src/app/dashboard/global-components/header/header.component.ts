import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { ModalComponent } from 'src/app/core/modal/modal.component';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css'],
})
export class HeaderComponent implements OnInit {
  bsModalRef: BsModalRef;
  perfilUsuario: any;

  constructor(private route: Router, private modalService: BsModalService) {}

  ngOnInit(): void {}

  /***
   * Lanzar los datos del perfil capturandolos del Localstorage y ajustandolos en HTML
   */

  cargarDataPerfil() {
    this.perfilUsuario = JSON.parse(localStorage.getItem('datos-usuario'));
    this.openModalWithComponent(this.perfilUsuario);
  }

  openModalWithComponent(perfilUsuario) {
    const initialState = {
      title: `Perfil del usuario`,
      list: `
            <div class="row">
              <div class="col-md-4"></div>
              <div class="col-md-4">
                <img width="145" alt="Perfil" src="assets/imagenes/perfilUsuario.png" />
              </div>
              <div class="col-md-4"></div>
            </div>
            <br>
            <div class="row">
              <div class="col-md-12">
                <b>Numero identificación: </b>${perfilUsuario.numeroidentificacion}<br>
                <b>Nombres: </b> ${perfilUsuario.nombre} ${perfilUsuario.primerapellido} ${perfilUsuario.segundoapellido}<br>
                <b>Dirección: </b>${perfilUsuario.direccion}<br>
                <b>Teléfono: </b>${perfilUsuario.correoElectronico}<br>
                <b>Correo electrónico: </b>${perfilUsuario.telefono}<br>
                <b>Fecha de nacimiento: </b>${perfilUsuario.fechanacimiento}<br>
                <b>Fecha de inscripción: </b>${perfilUsuario.fechainscripcion}<br>
                <b>Género: </b>${perfilUsuario.genero}<br>
              </div>
            </div>
            
            `,
    };
    this.bsModalRef = this.modalService.show(ModalComponent, { initialState });
    this.bsModalRef.content.closeBtnName = 'Cerrar';
  }

  cerrarSesion() {
    localStorage.clear();
    this.route.navigate(['/inicio-sesion']);
  }
}
