import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router'

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {
  menuArray: Array<any>;

  constructor(
    private _router: Router
  ) {
    this.menuArray = [
      { goToComponent: 'home', icono: 'fa-home', tooltipText: 'Inicio' },
      { goToComponent: 'grupos-investigacion', icono: 'fa-users', tooltipText: 'Grupos investigación' },
      { goToComponent: 'asignar-grupos', icono: 'fa-people-arrows', tooltipText: 'Asignar grupos'  },
      { goToComponent: 'ingestigadores', icono: 'fa-user-tie', tooltipText: 'Investigadores'  },
      { goToComponent: 'produccion', icono: 'fa-chalkboard-teacher', tooltipText: 'Producción'  },
      { goToComponent: 'semilleros', icono: 'fa-seedling', tooltipText: 'Semilleros'  },
    ]
  }

  ngOnInit(): void {
  }

  goToComponent(component) {
    this._router.navigate([component]);
  }

}
