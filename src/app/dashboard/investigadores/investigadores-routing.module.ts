import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { InvestigadoresComponent } from './investigadores.component';

const routes: Routes = [
  {
    path: '',
    component: InvestigadoresComponent,
  },
  {
    path: 'historial-investigador',
    //component: CrearGrupoComponent
    loadChildren: () => import('./historial-investigador/historial-investigador.module').then(m => m.HistorialInvestigadorModule)
  },
  {
    path: 'historial-conferencias',
    loadChildren: () => import('./historial-conferencias/historial-conferencias.module').then(m => m.HistorialConferenciasModule)
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class InvestigadoresRoutingModule {}
