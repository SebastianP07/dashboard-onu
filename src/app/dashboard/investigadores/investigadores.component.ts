import { Component, OnInit, TemplateRef } from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { ModalComponent } from 'src/app/core/modal/modal.component';
import { Router } from '@angular/router';

@Component({
  selector: 'app-investigadores',
  templateUrl: './investigadores.component.html',
  styleUrls: ['./investigadores.component.css']
})
export class InvestigadoresComponent implements OnInit {
  cardArray: Array<any>;

  constructor(
    private router: Router
  ) {
    this.cardArray = [
      { texto: 'Historial dedicación', icono: 'history', componente: 'ingestigadores/historial-investigador' },
      { texto: 'Conferencias', icono: 'microphone-alt', componente: 'ingestigadores/historial-conferencias' },
    ]
  }

  ngOnInit(): void {
    
  }

  goToComponent(componente) {
    this.router.navigate([componente])
  }


  goToBackComponent() {
    this.router.navigate(['home']);
  }
}
