import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HistorialConferenciasComponent } from './historial-conferencias.component';

xdescribe('HistorialConferenciasComponent', () => {
  let component: HistorialConferenciasComponent;
  let fixture: ComponentFixture<HistorialConferenciasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HistorialConferenciasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HistorialConferenciasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
