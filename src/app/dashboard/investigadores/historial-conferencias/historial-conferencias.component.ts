import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-historial-conferencias',
  templateUrl: './historial-conferencias.component.html',
  styleUrls: ['./historial-conferencias.component.css']
})
export class HistorialConferenciasComponent implements OnInit {
  cardArray: Array<any>;

  constructor(private router: Router) { 
    this.cardArray = [
      { texto: 'Agregar historial', icono: 'plus-circle', componente: 'ingestigadores/historial-conferencias/agregar-historial-conferencia' },
      { texto: 'Ver historial', icono: 'list', componente: 'ingestigadores/historial-conferencias/ver-historial-conferencia' },
    ]
   }

  ngOnInit(): void {
  }

  goToComponent(componente) {
    this.router.navigate([componente])
  }

  goToBackComponent() {
    this.router.navigate(['ingestigadores']);
  }

}
