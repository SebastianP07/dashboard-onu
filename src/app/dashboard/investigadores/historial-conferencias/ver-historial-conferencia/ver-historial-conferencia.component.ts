import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { InvestigadorServices } from 'src/app/core/services/investigador.service';

@Component({
  selector: 'app-ver-historial-conferencia',
  templateUrl: './ver-historial-conferencia.component.html',
  styleUrls: ['./ver-historial-conferencia.component.css'],
  providers: [InvestigadorServices]
})
export class VerHistorialConferenciaComponent implements OnInit {
  dtOptions: DataTables.Settings = {};
  dataTable: any [];

  constructor(private investigadorServices: InvestigadorServices, private router: Router) {
    this.cargarGruposRegistrados();
  }

  ngOnInit(): void {
    this.dtOptions = {
      pagingType: 'full_numbers',
      processing: true,
      pageLength: 2,
      dom: 'Bfrtip',
    };
  }

  cargarGruposRegistrados() {
    this.investigadorServices.verHistorialCompletoInvestigadores().subscribe(
      (response) => {
        console.log('response verGruposRegistrados => ', response);
        if (response.codigo == 1) {
          this.dataTable = response.objetoRespuestaArrayHistorialConferenciasInvestigadorAlmacenado;
          console.log(this.dataTable);
        } else {
          console.error(
            'error verGruposRegistrados => ',
            response.descripcionError
          );
        }
      },
      (error) => {
        console.error('error verGruposRegistrados => ', error);
      }
    );
  }
  
  goToBackComponent() {
    this.router.navigate(['ingestigadores/historial-conferencias']);
  }
}
