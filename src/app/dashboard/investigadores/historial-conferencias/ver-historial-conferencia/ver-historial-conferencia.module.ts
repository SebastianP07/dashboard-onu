import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { VerHistorialConferenciaRoutingModule } from './ver-historial-conferencia-routing.module';
import { VerHistorialConferenciaComponent } from './ver-historial-conferencia.component';
import { GlobalComponent } from 'src/app/dashboard/global-components/global-components';


@NgModule({
  declarations: [VerHistorialConferenciaComponent],
  imports: [
    CommonModule,
    VerHistorialConferenciaRoutingModule,
    GlobalComponent
  ]
})
export class VerHistorialConferenciaModule { }
