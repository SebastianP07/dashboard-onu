import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VerHistorialConferenciaComponent } from './ver-historial-conferencia.component';

xdescribe('VerHistorialConferenciaComponent', () => {
  let component: VerHistorialConferenciaComponent;
  let fixture: ComponentFixture<VerHistorialConferenciaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VerHistorialConferenciaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VerHistorialConferenciaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
