import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { VerHistorialConferenciaComponent } from './ver-historial-conferencia.component';

const routes: Routes = [{ path: '', component: VerHistorialConferenciaComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class VerHistorialConferenciaRoutingModule { }
