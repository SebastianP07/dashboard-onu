import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HistorialConferenciasRoutingModule } from './historial-conferencias-routing.module';
import { HistorialConferenciasComponent } from './historial-conferencias.component';


@NgModule({
  declarations: [HistorialConferenciasComponent],
  imports: [
    CommonModule,
    HistorialConferenciasRoutingModule
  ]
})
export class HistorialConferenciasModule { }
