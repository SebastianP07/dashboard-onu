import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { DatePipe } from '@angular/common';
import { defineLocale, esLocale } from 'ngx-bootstrap/chronos';
import { InvestigadoresData } from 'src/app/core/models/endpoints';
import { InvestigadorServices } from 'src/app/core/services/investigador.service';
import { ModalComponent } from 'src/app/core/modal/modal.component';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';

@Component({
  selector: 'app-agregar-historial-conferencia',
  templateUrl: './agregar-historial-conferencia.component.html',
  styleUrls: ['./agregar-historial-conferencia.component.css'],
  providers: [InvestigadorServices]
})
export class AgregarHistorialConferenciaComponent implements OnInit {
  ciudades: any;
  dataForm: FormGroup;
  datePipe = new DatePipe('en-US');
  investigadores: any[];
  bsModalRef: BsModalRef;
  
  constructor(
    private router: Router, 
    private formBuilder: FormBuilder, 
    private investigadorServices: InvestigadorServices,
    private modalService: BsModalService
    ) { 
    defineLocale('es', esLocale);
    
    this.dataForm = this.formBuilder.group({
      idMiembro: new FormControl(
        // Valor por defecto
        '',
        // Conjunto de validaciones
        Validators.compose([
          Validators.required
        ])
      ),
      idCiudad: new FormControl(
        // Valor por defecto
        '',
        // Conjunto de validaciones
        Validators.compose([
          Validators.required
        ])
      ),
      fecha: new FormControl(
        // Valor por defecto
        '',
        // Conjunto de validaciones
        Validators.compose([
          Validators.required
        ])
      ),

    });
  }

  ngOnInit(): void {
    this.ciudades = JSON.parse(localStorage.getItem("Ciudades"));
    this.obtenerMiemborsGrupo();
  }

  goToBackComponent() {
    this.router.navigate(['ingestigadores/historial-conferencias']);
  }

  submitDataForm(dataForm){
    console.log(dataForm);
    dataForm.fecha = this.datePipe.transform(
      dataForm.fecha,
      'dd/MM/yyyy'
    );
    this.investigadorServices
      .registrarHistorialConferencia(dataForm)
      .subscribe(
        (response) => {
          console.log('response registrarHistorialConferencia => ', response);
          if (response.codigo == 1) {
            this.dataForm.reset();
            this.openModalWithComponent(
              'Registro historial conferencia',
              response.descripcion
            );
          } else {
            this.dataForm.reset();
            this.openModalWithComponent(
              'Error al crear el Registro historial conferencia',
              response.descripcion
            );
            console.error(
              'error registrarHistorialConferencia => ',
              response.descripcionError
            );
          }
        },
        (error) => {
          this.dataForm.reset();
          console.error('error registrarHistorialConferencia => ', error);
        }
      );
  }

  obtenerMiemborsGrupo(){
    this.investigadorServices.obtenerMiemborsGrupo().subscribe(
      response => {
        console.log("response obtenerMiemborsGrupo => ", response);
        if (response.codigo == 1) {
          const resultado = [];
          for (let i = 0; i < InvestigadoresData.length; i++) {
            for (let j = 0; j < response.idMiembosGrupo.length; j++) {
              if(InvestigadoresData[i].id == response.idMiembosGrupo[j]){
                resultado.push(InvestigadoresData[i]);
              }
            }
          }
          this.investigadores = resultado;
        } else {
          console.error("error obtenerMiemborsGrupo => ", response.descripcionError);
        }
      },
      error => {
        this.dataForm.reset();
        console.error("error obtenerMiemborsGrupo => ", error);
      }
);
  }

  openModalWithComponent(titulo, mensaje) {
    const initialState = {
      title: `${titulo}`,
      list: `${mensaje}`,
    };
    this.bsModalRef = this.modalService.show(ModalComponent, { initialState });
    this.bsModalRef.content.closeBtnName = 'Cerrar';
  }

}
