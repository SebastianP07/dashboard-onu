import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AgregarHistorialConferenciaComponent } from './agregar-historial-conferencia.component';

const routes: Routes = [{ path: '', component: AgregarHistorialConferenciaComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AgregarHistorialConferenciaRoutingModule { }
