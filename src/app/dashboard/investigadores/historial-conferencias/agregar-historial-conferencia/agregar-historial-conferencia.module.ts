import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AgregarHistorialConferenciaRoutingModule } from './agregar-historial-conferencia-routing.module';
import { AgregarHistorialConferenciaComponent } from './agregar-historial-conferencia.component';
import { ReactiveFormsModule } from '@angular/forms';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';


@NgModule({
  declarations: [AgregarHistorialConferenciaComponent],
  imports: [
    CommonModule,
    AgregarHistorialConferenciaRoutingModule,
    ReactiveFormsModule,
    BsDatepickerModule.forRoot()
  ]
})
export class AgregarHistorialConferenciaModule { }
