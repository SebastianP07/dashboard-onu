import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AgregarHistorialConferenciaComponent } from './agregar-historial-conferencia.component';

xdescribe('AgregarHistorialConferenciaComponent', () => {
  let component: AgregarHistorialConferenciaComponent;
  let fixture: ComponentFixture<AgregarHistorialConferenciaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AgregarHistorialConferenciaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AgregarHistorialConferenciaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
