import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HistorialConferenciasComponent } from './historial-conferencias.component';

const routes: Routes = [
  { path: '', component: HistorialConferenciasComponent },
  {
    path: 'agregar-historial-conferencia',
    //component: CrearGrupoComponent
    loadChildren: () => import('./agregar-historial-conferencia/agregar-historial-conferencia.module').then(m => m.AgregarHistorialConferenciaModule)
  },
  {
    path: 'ver-historial-conferencia',
    loadChildren: () => import('./ver-historial-conferencia/ver-historial-conferencia.module').then(m => m.VerHistorialConferenciaModule)
  },

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class HistorialConferenciasRoutingModule {}
