import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { InvestigadoresRoutingModule } from './investigadores-routing.module';
import { InvestigadoresComponent } from './investigadores.component';
import { GlobalComponent } from '../global-components/global-components';


@NgModule({
  declarations: [InvestigadoresComponent],
  imports: [
    CommonModule,
    InvestigadoresRoutingModule,
    GlobalComponent
  ]
})
export class InvestigadoresModule { }
