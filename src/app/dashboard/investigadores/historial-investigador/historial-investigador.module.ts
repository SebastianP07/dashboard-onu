import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HistorialInvestigadorRoutingModule } from './historial-investigador-routing.module';
import { HistorialInvestigadorComponent } from './historial-investigador.component';


@NgModule({
  declarations: [HistorialInvestigadorComponent],
  imports: [
    CommonModule,
    HistorialInvestigadorRoutingModule
  ]
})
export class HistorialInvestigadorModule { }
