import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HistorialInvestigadorComponent } from './historial-investigador.component';

const routes: Routes = [
  { path: '', component: HistorialInvestigadorComponent },
  {
    path: 'agregar-historial-investigador',
    //component: CrearGrupoComponent
    loadChildren: () => import('./agregar-historial-investigador/agregar-historial-investigador.module').then(m => m.AgregarHistorialInvestigadorModule)
  },
  {
    path: 'ver-historial-investigador',
    loadChildren: () => import('./ver-historial-investigador/ver-historial-investigador.module').then(m => m.VerHistorialInvestigadorModule)
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HistorialInvestigadorRoutingModule { }
