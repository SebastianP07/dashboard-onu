import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AgregarHistorialInvestigadorRoutingModule } from './agregar-historial-investigador-routing.module';
import { AgregarHistorialInvestigadorComponent } from './agregar-historial-investigador.component';
import { ReactiveFormsModule } from '@angular/forms';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';


@NgModule({
  declarations: [AgregarHistorialInvestigadorComponent],
  imports: [
    CommonModule,
    AgregarHistorialInvestigadorRoutingModule,
    ReactiveFormsModule,
    BsDatepickerModule.forRoot()
  ]
})
export class AgregarHistorialInvestigadorModule { }
