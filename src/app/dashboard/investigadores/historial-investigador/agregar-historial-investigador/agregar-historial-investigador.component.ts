import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { InvestigadoresData } from 'src/app/core/models/endpoints';
import {
  FormGroup,
  FormBuilder,
  FormControl,
  Validators,
} from '@angular/forms';
import { DatePipe } from '@angular/common';
import { defineLocale, esLocale } from 'ngx-bootstrap/chronos';
import { InvestigadorServices } from 'src/app/core/services/investigador.service';
import { ModalComponent } from 'src/app/core/modal/modal.component';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';

@Component({
  selector: 'app-agregar-historial-investigador',
  templateUrl: './agregar-historial-investigador.component.html',
  styleUrls: ['./agregar-historial-investigador.component.css'],
  providers: [InvestigadorServices],
})
export class AgregarHistorialInvestigadorComponent implements OnInit {
  investigadores: any[];
  crearForm: FormGroup;
  datePipe = new DatePipe('en-US');
  bsModalRef: BsModalRef;

  constructor(
    private router: Router,
    private formBuilder: FormBuilder,
    private investigadorServices: InvestigadorServices,
    private modalService: BsModalService
  ) {
    defineLocale('es', esLocale);

    this.crearForm = this.formBuilder.group({
      idMiembro: new FormControl(
        // Valor por defecto
        '',
        // Conjunto de validaciones
        Validators.compose([Validators.required])
      ),
      actividad: new FormControl(
        // Valor por defecto
        '',
        // Conjunto de validaciones
        Validators.compose([Validators.required])
      ),
      fechaInicio: new FormControl(
        // Valor por defecto
        '',
        // Conjunto de validaciones
        Validators.compose([Validators.required])
      ),
      fechaFin: new FormControl(
        // Valor por defecto
        '',
        // Conjunto de validaciones
        Validators.compose([Validators.required])
      ),
      horasDedicadas: new FormControl(
        // Valor por defecto
        '',
        // Conjunto de validaciones
        Validators.compose([Validators.required])
      ),
    });
  }

  ngOnInit(): void {
    this.obtenerMiemborsGrupo();
  }

  goToBackComponent() {
    this.router.navigate(['ingestigadores/historial-investigador']);
  }

  openModalWithComponent(titulo, mensaje) {
    const initialState = {
      title: `${titulo}`,
      list: `${mensaje}`,
    };
    this.bsModalRef = this.modalService.show(ModalComponent, { initialState });
    this.bsModalRef.content.closeBtnName = 'Cerrar';
  }

  submitcrearForm(crearForm) {
    crearForm.fechaInicio = this.datePipe.transform(
      crearForm.fechaInicio,
      'dd/MM/yyyy'
    );
    crearForm.fechaFin = this.datePipe.transform(
      crearForm.fechaFin,
      'dd/MM/yyyy'
    );
    crearForm.horasDedicadas = crearForm.horasDedicadas.toString();

    console.log(crearForm);
    this.investigadorServices
      .registrarHistorialInvestigador(crearForm)
      .subscribe(
        (response) => {
          console.log('response registrarHistorialInvestigador => ', response);
          if (response.codigo == 1) {
            this.crearForm.reset();
            this.openModalWithComponent(
              'Registro historial investigador',
              response.descripcion
            );
          } else {
            this.crearForm.reset();
            this.openModalWithComponent(
              'Error al crear el Registro historial investigador',
              response.descripcion
            );
            console.error(
              'error registrarHistorialInvestigador => ',
              response.descripcionError
            );
          }
        },
        (error) => {
          this.crearForm.reset();
          console.error('error registrarHistorialInvestigador => ', error);
        }
      );
  }

  obtenerMiemborsGrupo() {
    this.investigadorServices.obtenerMiemborsGrupo().subscribe(
      (response) => {
        console.log('response obtenerMiemborsGrupo => ', response);
        if (response.codigo == 1) {
          const resultado = [];
          for (let i = 0; i < InvestigadoresData.length; i++) {
            for (let j = 0; j < response.idMiembosGrupo.length; j++) {
              if (InvestigadoresData[i].id == response.idMiembosGrupo[j]) {
                resultado.push(InvestigadoresData[i]);
              }
            }
          }
          this.investigadores = resultado;
        } else {
          console.error(
            'error obtenerMiemborsGrupo => ',
            response.descripcionError
          );
        }
      },
      (error) => {
        this.crearForm.reset();
        console.error('error obtenerMiemborsGrupo => ', error);
      }
    );
  }
}
