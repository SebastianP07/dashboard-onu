import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AgregarHistorialInvestigadorComponent } from './agregar-historial-investigador.component';

xdescribe('AgregarHistorialInvestigadorComponent', () => {
  let component: AgregarHistorialInvestigadorComponent;
  let fixture: ComponentFixture<AgregarHistorialInvestigadorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AgregarHistorialInvestigadorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AgregarHistorialInvestigadorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
