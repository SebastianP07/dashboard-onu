import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-historial-investigador',
  templateUrl: './historial-investigador.component.html',
  styleUrls: ['./historial-investigador.component.css']
})
export class HistorialInvestigadorComponent implements OnInit {
  cardArray: Array<any>;

  constructor(private router: Router) {
    this.cardArray = [
      { texto: 'Agregar historial', icono: 'plus-circle', componente: 'ingestigadores/historial-investigador/agregar-historial-investigador' },
      { texto: 'Ver historial', icono: 'list', componente: 'ingestigadores/historial-investigador/ver-historial-investigador' },
    ]
   }

  ngOnInit(): void {
  }

  goToComponent(componente) {
    this.router.navigate([componente])
  }

  goToBackComponent() {
    this.router.navigate(['ingestigadores']);
  }
}
