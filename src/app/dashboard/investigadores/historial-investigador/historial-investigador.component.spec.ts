import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HistorialInvestigadorComponent } from './historial-investigador.component';

xdescribe('HistorialInvestigadorComponent', () => {
  let component: HistorialInvestigadorComponent;
  let fixture: ComponentFixture<HistorialInvestigadorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HistorialInvestigadorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HistorialInvestigadorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
