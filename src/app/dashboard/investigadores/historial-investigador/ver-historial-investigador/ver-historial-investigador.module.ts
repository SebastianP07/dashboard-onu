import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { VerHistorialInvestigadorRoutingModule } from './ver-historial-investigador-routing.module';
import { VerHistorialInvestigadorComponent } from './ver-historial-investigador.component';
import { GlobalComponent } from 'src/app/dashboard/global-components/global-components';


@NgModule({
  declarations: [VerHistorialInvestigadorComponent],
  imports: [
    CommonModule,
    VerHistorialInvestigadorRoutingModule,
    GlobalComponent
  ]
})
export class VerHistorialInvestigadorModule { }
