import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VerHistorialInvestigadorComponent } from './ver-historial-investigador.component';

xdescribe('VerHistorialInvestigadorComponent', () => {
  let component: VerHistorialInvestigadorComponent;
  let fixture: ComponentFixture<VerHistorialInvestigadorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VerHistorialInvestigadorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VerHistorialInvestigadorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
