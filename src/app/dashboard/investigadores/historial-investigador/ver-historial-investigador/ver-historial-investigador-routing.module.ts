import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { VerHistorialInvestigadorComponent } from './ver-historial-investigador.component';

const routes: Routes = [{ path: '', component: VerHistorialInvestigadorComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class VerHistorialInvestigadorRoutingModule { }
