import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { GrupoServices } from 'src/app/core/services/grupo.services';
import { InvestigadorServices } from 'src/app/core/services/investigador.service';

@Component({
  selector: 'app-ver-historial-investigador',
  templateUrl: './ver-historial-investigador.component.html',
  styleUrls: ['./ver-historial-investigador.component.css'],
  providers: [InvestigadorServices]
})
export class VerHistorialInvestigadorComponent implements OnInit {
  dtOptions: DataTables.Settings = {};
  dataTable: any [];

  constructor(private investigadorServices: InvestigadorServices, private router: Router) {
    this.cargarGruposRegistrados();
  }

  ngOnInit(): void {
    this.dtOptions = {
      pagingType: 'full_numbers',
      processing: true,
      pageLength: 2,
      dom: 'Bfrtip',
    };
  }

  cargarGruposRegistrados() {
    this.investigadorServices.verHistorialCompletoInvestigadores().subscribe(
      (response) => {
        console.log('response verGruposRegistrados => ', response);
        if (response.codigo == 1) {
          this.dataTable = response.objetoRespuestaArrayHistorialInvestigadorGrupoAlmacenado;
          console.log(this.dataTable);
        } else {
          console.error(
            'error verGruposRegistrados => ',
            response.descripcionError
          );
        }
      },
      (error) => {
        console.error('error verGruposRegistrados => ', error);
      }
    );
  }

  goToBackComponent() {
    this.router.navigate(['ingestigadores/historial-investigador']);
  }
}
