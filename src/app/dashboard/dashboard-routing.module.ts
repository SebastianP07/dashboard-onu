import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { InicioSesionGuard } from '../core/guards/inicio-sesion.guard';
import { DashboardComponent } from './dashboard.component';

const routes: Routes = [
  {
    path: '',
    component: DashboardComponent,
    children: [
      {
        path: '',
        redirectTo: 'home',
        pathMatch: 'prefix'
      },
      {
        path: 'home',
        loadChildren: () => import('./home/home.module').then(m => m.HomeModule)
      },
      {
        path: 'grupos-investigacion',
        loadChildren: () => import('./grupos-investigacion/grupos-investigacion.module').then(m => m.GruposInvestigacionModule)
      },
      {
        path: 'asignar-grupos',
        loadChildren: () => import('./asignar-grupos/asignar-grupos.module').then(m => m.AsignarGruposModule)
      },
      {
        path: 'ingestigadores',
        loadChildren: () => import('./investigadores/investigadores.module').then(m => m.InvestigadoresModule)
      },
      {
        path: 'produccion',
        loadChildren: () => import('./produccion/produccion.module').then(m => m.ProduccionModule)
      },
      {
        path: 'semilleros',
        loadChildren: () => import('./semilleros/semilleros.module').then(m => m.SemillerosModule)
      },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DashboardRoutingModule { }
