import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EditarGrupoComponent } from './editar-grupo.component';


const routes: Routes = [
  {path: '', component: EditarGrupoComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EditarGrupoRoutingModule { }
