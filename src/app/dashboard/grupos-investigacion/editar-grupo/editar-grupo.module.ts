import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EditarGrupoRoutingModule } from './editar-grupo-routing.module';
import { EditarGrupoComponent } from './editar-grupo.component';
import { GlobalComponent } from '../../global-components/global-components';


@NgModule({
  declarations: [EditarGrupoComponent],
  imports: [
    CommonModule,
    EditarGrupoRoutingModule,
    GlobalComponent
  ]
})
export class EditarGrupoModule { }
