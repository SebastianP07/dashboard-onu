import { Component, OnInit } from '@angular/core';
import { UserServices } from "../../core/services/user.service";
import { Usuario } from "../../core/models/Usuario";
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-grupos-investigacion',
  templateUrl: './grupos-investigacion.component.html',
  styleUrls: ['./grupos-investigacion.component.css'],
  providers: [UserServices]
})
export class GruposInvestigacionComponent implements OnInit {
  cardArray: Array<any>;

  constructor(
    private router: Router
  ) {
    this.cardArray = [
      { texto: 'Crear grupo', icono: 'plus-circle', componente: 'grupos-investigacion/crear-grupo' },
      { texto: 'Editar grupo', icono: 'edit', componente: 'grupos-investigacion/editar-grupo' },
      { texto: 'Eliminar grupo', icono: 'trash-alt', componente: 'grupos-investigacion/eliminar-grupo' },
      { texto: 'Ver grupos', icono: 'list', componente: 'grupos-investigacion/ver-grupo' }
    ]
  }

  ngOnInit(): void {
  }

  goToComponent(componente) {
    this.router.navigate([componente])
  }

  goToBackComponent() {
    this.router.navigate(['home']);
  }

}
