import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { GruposInvestigacionRoutingModule } from './grupos-investigacion-routing.module';
import { GruposInvestigacionComponent } from './grupos-investigacion.component';


@NgModule({
  declarations: [GruposInvestigacionComponent],
  imports: [
    CommonModule,
    GruposInvestigacionRoutingModule
  ]
})
export class GruposInvestigacionModule { }
