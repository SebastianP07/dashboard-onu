import { Component, OnInit, TemplateRef } from '@angular/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { GrupoServices } from 'src/app/core/services/grupo.services';
import { Router } from '@angular/router';

@Component({
  selector: 'app-eliminar-grupo',
  templateUrl: './eliminar-grupo.component.html',
  styleUrls: ['./eliminar-grupo.component.css'],
  providers: [GrupoServices],
})
export class EliminarGrupoComponent implements OnInit {
  dataTable: any;
  modalRef: BsModalRef;
  dtOptions: DataTables.Settings = {};
  dataModal: any;
  
  constructor(
    private modalService: BsModalService,
    private grupoServices: GrupoServices,
    private router: Router
  ) {
    this.cargarGruposRegistrados();
  }

  ngOnInit(): void {
    this.dtOptions = {
      pagingType: 'full_numbers',
      processing: true,
      pageLength: 2,
      dom: 'Bfrtip',
    };
  }

  cargarGruposRegistrados() {
    this.grupoServices.verGruposRegistrados().subscribe(
      (response) => {
        console.log('response verGruposRegistrados => ', response);
        if (response.codigo == 1) {
          this.dataTable = response.objetoRespuestaArrayGrupoInvestigacion;
          console.log(this.dataTable);
        } else {
          console.error(
            'error verGruposRegistrados => ',
            response.descripcionError
          );
        }
      },
      (error) => {
        console.error('error verGruposRegistrados => ', error);
      }
    );
  }

  openModal(template: TemplateRef<any>, idRegistro) {
    this.dataModal = idRegistro;


    this.modalRef = this.modalService.show(template, {
      ariaDescribedby: 'my-modal-description',
      ariaLabelledBy: 'my-modal-title'
    });
  }

  actualizarRegistro(idRegistro){
    alert(idRegistro);
    this.modalRef.hide();
  }

  goToBackComponent() {
    this.router.navigate(['grupos-investigacion']);
  }
}
