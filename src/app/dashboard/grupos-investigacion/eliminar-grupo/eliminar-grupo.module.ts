import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EliminarGrupoRoutingModule } from './eliminar-grupo-routing.module';
import { EliminarGrupoComponent } from './eliminar-grupo.component';
import { GlobalComponent } from '../../global-components/global-components';


@NgModule({
  declarations: [EliminarGrupoComponent],
  imports: [
    CommonModule,
    EliminarGrupoRoutingModule,
    GlobalComponent
  ]
})
export class EliminarGrupoModule { }
