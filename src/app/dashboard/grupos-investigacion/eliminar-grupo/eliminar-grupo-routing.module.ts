import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EliminarGrupoComponent } from './eliminar-grupo.component';


const routes: Routes = [
  {path: '', component: EliminarGrupoComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EliminarGrupoRoutingModule { }
