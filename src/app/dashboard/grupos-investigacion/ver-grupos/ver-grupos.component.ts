import { Component, OnInit } from '@angular/core';
import { GrupoServices } from 'src/app/core/services/grupo.services';
import { Router } from '@angular/router';

@Component({
  selector: 'app-ver-grupos',
  templateUrl: './ver-grupos.component.html',
  styleUrls: ['./ver-grupos.component.css'],
  providers: [GrupoServices],
})
export class VerGruposComponent implements OnInit {
  dtOptions: DataTables.Settings = {};
  dataTable: any [];

  constructor(private grupoServices: GrupoServices, private router: Router) {
    this.cargarGruposRegistrados();
  }

  ngOnInit(): void {
    this.dtOptions = {
      pagingType: 'full_numbers',
      processing: true,
      pageLength: 2,
      dom: 'Bfrtip',
    };
  }

  cargarGruposRegistrados() {
    this.grupoServices.verGruposRegistrados().subscribe(
      (response) => {
        console.log('response verGruposRegistrados => ', response);
        if (response.codigo == 1) {
          this.dataTable = response.objetoRespuestaArrayGrupoInvestigacion;
          console.log(this.dataTable);
        } else {
          console.error(
            'error verGruposRegistrados => ',
            response.descripcionError
          );
        }
      },
      (error) => {
        console.error('error verGruposRegistrados => ', error);
      }
    );
  }

  goToBackComponent() {
    this.router.navigate(['grupos-investigacion']);
  }
}
