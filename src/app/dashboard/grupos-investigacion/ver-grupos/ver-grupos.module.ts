import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { VerGruposRoutingModule } from './ver-grupos-routing.module';
import { VerGruposComponent } from './ver-grupos.component';
import { GlobalComponent } from '../../global-components/global-components';


@NgModule({
  declarations: [VerGruposComponent],
  imports: [
    CommonModule,
    VerGruposRoutingModule,
    GlobalComponent
  ]
})
export class VerGruposModule { }
