import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CrearGrupoRoutingModule } from './crear-grupo-routing.module';
import { CrearGrupoComponent } from './crear-grupo.component';
import { ReactiveFormsModule } from '@angular/forms';
import { GlobalComponent } from '../../global-components/global-components';



@NgModule({
  declarations: [CrearGrupoComponent],
  imports: [
    CommonModule,
    CrearGrupoRoutingModule,
    ReactiveFormsModule,
    GlobalComponent
  ]
})
export class CrearGrupoModule { }
