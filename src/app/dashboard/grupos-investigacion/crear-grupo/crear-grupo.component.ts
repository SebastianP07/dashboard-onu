import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { InvestigadoresData } from "../../../core/models/endpoints";
import { DatePipe } from '@angular/common';
import { GrupoServices } from 'src/app/core/services/grupo.services';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { ModalComponent } from 'src/app/core/modal/modal.component';
import { Router } from '@angular/router';
@Component({
  selector: 'app-crear-grupo',
  templateUrl: './crear-grupo.component.html',
  styleUrls: ['./crear-grupo.component.css'],
  providers: [GrupoServices]
})
export class CrearGrupoComponent implements OnInit {
  grupoForm: FormGroup;
  datePipe = new DatePipe('en-US');
  miembrosGrupo: any;
  bsModalRef: BsModalRef;
  
  ciudades: any[];
  areaConocimiento: any[];
  lineasInvestigacion: any[];
  instituciones: any[];
  investigadores: any[];
  
  constructor(
    private formBuilder: FormBuilder,
    private grupoServices: GrupoServices,
    private modalService: BsModalService,
    private router: Router
  ) { 
    this.ciudades = JSON.parse(localStorage.getItem("Ciudades"));
    this.instituciones = JSON.parse(localStorage.getItem("Instituciones"));
    this.areaConocimiento = JSON.parse(localStorage.getItem("AreaConocimiento"));
    this.lineasInvestigacion = JSON.parse(localStorage.getItem("LineasInvestigacion"));
    this.investigadores = InvestigadoresData;

    this.grupoForm = this.formBuilder.group({
      idInvestigador: new FormControl(
        // Valor por defecto
        '',
        // Conjunto de validaciones
        Validators.compose([
          Validators.required
        ])
      ),
      idCiudad: new FormControl(
        // Valor por defecto
        '',
        // Conjunto de validaciones
        Validators.compose([
          Validators.required
        ])
      ),
      paginaWeb: new FormControl(
        // Valor por defecto
        '',
        // Conjunto de validaciones
        Validators.compose([
          Validators.required
        ])
      ),
      email: new FormControl(
        // Valor por defecto
        '',
        // Conjunto de validaciones
        Validators.compose([
          Validators.required,
          Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')
        ])
      ),
      instituciones: new FormControl(
        // Valor por defecto
        '',
        // Conjunto de validaciones
        Validators.compose([
          Validators.required
        ])
      ),
      idAreaConocimiento: new FormControl(
        // Valor por defecto
        ''
      ),
      mesConformacion: new FormControl(
        // Valor por defecto
        this.datePipe.transform(new Date(), 'MM')
      ),
      anioConformacion: new FormControl(
        // Valor por defecto
        this.datePipe.transform(new Date(), 'yyyy')
      ),
      miembrosGrupoLider: new FormControl(
        // Valor por defecto
        ''
      ),
      miembrosGrupo: new FormControl(
        // Valor por defecto
        ''
      ),
      asignarInvestigadores: new FormControl(
        // Valor por defecto
        '',
        // Conjunto de validaciones
        Validators.compose([
          Validators.required
        ])
      ),
    });
  }

  ngOnInit(): void { }

  submitGrupoForm(datosGrupo){
    datosGrupo.miembrosGrupoLider = {
      idInvestigador: datosGrupo.idInvestigador,
      idRol: '1',
      fechaVinculacion: this.datePipe.transform(new Date(), 'dd/MM/yyyy')
    };

    datosGrupo.miembrosGrupo = {
      idInvestigador: datosGrupo.asignarInvestigadores.filter(elemento => elemento != Number(datosGrupo.idInvestigador)).toString(),
      idRol: '2',
      fechaVinculacion: this.datePipe.transform(new Date(), 'dd/MM/yyyy')
    };

    datosGrupo.instituciones = {
      idInstituciones: datosGrupo.instituciones.toString()
    };

    console.log(datosGrupo);

    this.grupoServices.registrarGrupo(datosGrupo).subscribe(
      response => {
        console.log("response obtenerDataGlobal => ", response);
        if (response.codigo == 1) {
          this.grupoForm.reset();
          this.openModalWithComponent('Creación de grupo', response.descripcion);
        } else {
          this.grupoForm.reset();
          this.openModalWithComponent('Error al crear el grupo', response.descripcion);
          console.error("error registrarUsuario => ", response.descripcionError);
        }
      },
      error => {
        this.grupoForm.reset();
        console.error("error registrarUsuario => ", error);
      }
    );
  }


  openModalWithComponent(titulo, mensaje) {
    const initialState = {
      title: `${titulo}`,
      list: `${mensaje}`,
    };
    this.bsModalRef = this.modalService.show(ModalComponent, { initialState });
    this.bsModalRef.content.closeBtnName = 'Cerrar';
  }

  goToBackComponent() {
    this.router.navigate(['grupos-investigacion']);
  }
}
