import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CrearGrupoComponent } from './crear-grupo.component';


const routes: Routes = [
  {path:'', component: CrearGrupoComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CrearGrupoRoutingModule { }
