import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { GruposInvestigacionComponent } from './grupos-investigacion.component';
import { CrearGrupoComponent } from './crear-grupo/crear-grupo.component';


const routes: Routes = [
  {
    path: '',
    component: GruposInvestigacionComponent
  },
  {
    path: 'crear-grupo',
    //component: CrearGrupoComponent
    loadChildren: () => import('./crear-grupo/crear-grupo.module').then(m => m.CrearGrupoModule)
  },
  {
    path: 'editar-grupo',
    loadChildren: () => import('./editar-grupo/editar-grupo.module').then(m => m.EditarGrupoModule)
  },
  {
    path: 'eliminar-grupo',
    loadChildren: () => import('./eliminar-grupo/eliminar-grupo.module').then(m => m.EliminarGrupoModule)
  },
  {
    path: 'ver-grupo',
    loadChildren: () => import('./ver-grupos/ver-grupos.module').then(m => m.VerGruposModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class GruposInvestigacionRoutingModule { }
