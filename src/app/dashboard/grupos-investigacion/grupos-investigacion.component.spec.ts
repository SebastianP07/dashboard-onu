import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GruposInvestigacionComponent } from './grupos-investigacion.component';
import { UserServices } from 'src/app/core/services/user.service';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

xdescribe('GruposInvestigacionComponent', () => {
  let component: GruposInvestigacionComponent;
  let fixture: ComponentFixture<GruposInvestigacionComponent>;
 
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GruposInvestigacionComponent]
    })
    .compileComponents();
  }));
 
  beforeEach(() => {
    fixture = TestBed.createComponent(GruposInvestigacionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
  
});
