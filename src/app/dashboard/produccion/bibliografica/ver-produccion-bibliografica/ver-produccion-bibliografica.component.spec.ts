import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VerProduccionComponentBibliografica } from './ver-produccion-bibliografica.component';

xdescribe('VerProduccionComponent', () => {
  let component: VerProduccionComponentBibliografica;
  let fixture: ComponentFixture<VerProduccionComponentBibliografica>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VerProduccionComponentBibliografica ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VerProduccionComponentBibliografica);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
