import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { VerProduccionComponentBibliografica } from './ver-produccion-bibliografica.component';


const routes: Routes = [
  {path: '', component: VerProduccionComponentBibliografica}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class VerProduccionBibliograficaRoutingModule { }
