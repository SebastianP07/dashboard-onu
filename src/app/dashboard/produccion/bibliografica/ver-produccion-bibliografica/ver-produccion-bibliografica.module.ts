import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { VerProduccionBibliograficaRoutingModule } from './ver-produccion-bibliografica-routing.module';
import { VerProduccionComponentBibliografica } from './ver-produccion-bibliografica.component';


@NgModule({
  declarations: [VerProduccionComponentBibliografica],
  imports: [
    CommonModule,
    VerProduccionBibliograficaRoutingModule
  ]
})
export class VerProduccionBibliograficaModule { }
