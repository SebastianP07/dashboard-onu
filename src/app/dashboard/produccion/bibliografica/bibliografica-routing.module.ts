import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BibliograficaComponent } from './bibliografica.component';


const routes: Routes = [
  {
    path: '',
    component: BibliograficaComponent
  },
  {
    path: 'crear-produccion-bibliografica',
    loadChildren: () => import('./crear-produccion-bibliografica/crear-produccion-bibliografica.module').then(m => m.CrearProduccionBibliograficaModule)
  },
  {
    path: 'editar-produccion-bibliografica',
    loadChildren: () => import('./editar-produccion-bibliografica/editar-produccion-bibliografica.module').then(m => m.EditarProduccionBibliograficaModule)
  },
  {
    path: 'eliminar-produccion-bibliografica',
    loadChildren: () => import('./eliminar-produccion-bibliografica/eliminar-produccion-bibliografica.module').then(m => m.EliminarProduccionBibliograficaModule)
  },
  {
    path: 'ver-produccion-bibliografica',
    loadChildren: () => import('./ver-produccion-bibliografica/ver-produccion-bibliografica.module').then(m => m.VerProduccionBibliograficaModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BibliograficaRoutingModule { }
