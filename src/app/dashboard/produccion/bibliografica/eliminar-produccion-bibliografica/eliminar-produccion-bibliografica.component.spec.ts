import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EliminarProduccionComponentBibliografica } from './eliminar-produccion-bibliografica.component';

xdescribe('EliminarProduccionComponent', () => {
  let component: EliminarProduccionComponentBibliografica;
  let fixture: ComponentFixture<EliminarProduccionComponentBibliografica>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EliminarProduccionComponentBibliografica ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EliminarProduccionComponentBibliografica);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
