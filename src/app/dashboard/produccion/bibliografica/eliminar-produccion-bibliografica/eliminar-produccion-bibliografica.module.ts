import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EliminarProduccionBibliograficaRoutingModule } from './eliminar-produccion-bibliografica-routing.module';
import { EliminarProduccionComponentBibliografica } from './eliminar-produccion-bibliografica.component';


@NgModule({
  declarations: [EliminarProduccionComponentBibliografica],
  imports: [
    CommonModule,
    EliminarProduccionBibliograficaRoutingModule
  ]
})
export class EliminarProduccionBibliograficaModule { }
