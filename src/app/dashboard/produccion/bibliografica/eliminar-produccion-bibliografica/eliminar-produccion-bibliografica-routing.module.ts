import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EliminarProduccionComponentBibliografica } from './eliminar-produccion-bibliografica.component';


const routes: Routes = [
  {path: '', component: EliminarProduccionComponentBibliografica}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EliminarProduccionBibliograficaRoutingModule { }
