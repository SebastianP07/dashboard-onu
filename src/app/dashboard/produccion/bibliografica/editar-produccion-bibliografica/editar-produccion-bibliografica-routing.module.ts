import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EditarProduccionComponentBibliografica } from './editar-produccion-bibliografica.component';


const routes: Routes = [
  {path: '', component: EditarProduccionComponentBibliografica}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EditarProduccionBibliograficaRoutingModule { }
