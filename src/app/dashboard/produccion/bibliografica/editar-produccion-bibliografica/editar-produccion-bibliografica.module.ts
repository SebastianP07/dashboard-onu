import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EditarProduccionBibliograficaRoutingModule } from './editar-produccion-bibliografica-routing.module';
import { EditarProduccionComponentBibliografica } from './editar-produccion-bibliografica.component';


@NgModule({
  declarations: [EditarProduccionComponentBibliografica],
  imports: [
    CommonModule,
    EditarProduccionBibliograficaRoutingModule
  ]
})
export class EditarProduccionBibliograficaModule { }
