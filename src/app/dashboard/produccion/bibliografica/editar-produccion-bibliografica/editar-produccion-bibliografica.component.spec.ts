import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditarProduccionComponentBibliografica } from './editar-produccion-bibliografica.component';

xdescribe('EditarProduccionComponent', () => {
  let component: EditarProduccionComponentBibliografica;
  let fixture: ComponentFixture<EditarProduccionComponentBibliografica>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditarProduccionComponentBibliografica ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditarProduccionComponentBibliografica);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
