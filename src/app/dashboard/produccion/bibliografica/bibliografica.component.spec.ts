import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BibliograficaComponent } from './bibliografica.component';

xdescribe('BibliograficaComponent', () => {
  let component: BibliograficaComponent;
  let fixture: ComponentFixture<BibliograficaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BibliograficaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BibliograficaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
  
});
