import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CrearProduccionComponentBibliografica } from './crear-produccion-bibliografica.component';

xdescribe('CrearProduccionComponent', () => {
  let component: CrearProduccionComponentBibliografica;
  let fixture: ComponentFixture<CrearProduccionComponentBibliografica>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CrearProduccionComponentBibliografica ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CrearProduccionComponentBibliografica);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
