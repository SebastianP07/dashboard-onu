import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CrearProduccionComponentBibliografica } from './crear-produccion-bibliografica.component';


const routes: Routes = [
  { path: '', component: CrearProduccionComponentBibliografica }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CrearProduccionBibliograficaRoutingModule { }
