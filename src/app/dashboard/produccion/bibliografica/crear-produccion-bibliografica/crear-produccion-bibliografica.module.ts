import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CrearProduccionBibliograficaRoutingModule } from './crear-produccion-bibliografica-routing.module';
import { CrearProduccionComponentBibliografica } from './crear-produccion-bibliografica.component';


@NgModule({
  declarations: [CrearProduccionComponentBibliografica],
  imports: [
    CommonModule,
    CrearProduccionBibliograficaRoutingModule
  ]
})
export class CrearProduccionBibliograficaModule { }
