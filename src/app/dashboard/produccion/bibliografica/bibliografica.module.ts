import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BibliograficaRoutingModule } from './bibliografica-routing.module';
import { BibliograficaComponent } from './bibliografica.component';


@NgModule({
  declarations: [BibliograficaComponent],
  imports: [
    CommonModule,
    BibliograficaRoutingModule
  ]
})
export class BibliograficaModule { }
