import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-bibliografica',
  templateUrl: './bibliografica.component.html',
  styleUrls: ['./bibliografica.component.css']
})
export class BibliograficaComponent implements OnInit {
  cardArray: Array<any>;

  constructor(
    private router: Router
  ) {
    this.cardArray = [
      { texto: 'Crear producción', icono: 'plus-circle', componente: 'produccion/bibliografica/crear-produccion-bibliografica' },
      { texto: 'Editar producción', icono: 'edit', componente: 'produccion/bibliografica/editar-produccion-bibliografica' },
      { texto: 'Eliminar producción', icono: 'trash-alt', componente: 'produccion/bibliografica/eliminar-produccion-bibliografica' },
      { texto: 'Ver producción', icono: 'list', componente: 'produccion/bibliografica/ver-produccion-bibliografica' }
    ]
   }

  ngOnInit(): void {
  }

  goToComponent(componente) {
    this.router.navigate([componente])
  }

}
