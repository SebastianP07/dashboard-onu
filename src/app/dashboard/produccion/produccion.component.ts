import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-produccion',
  templateUrl: './produccion.component.html',
  styleUrls: ['./produccion.component.css']
})
export class ProduccionComponent implements OnInit {
  cardArray: Array<any>;

  constructor(
    private router: Router
  ) {
    this.cardArray = [
      { texto: 'Producción academica', icono: 'chalkboard', componente: 'produccion/academica' },
      //{ texto: 'Producción bibliográfica', icono: 'book', componente: 'produccion/bibliografica' },
      //{ texto: 'Producción técnica', icono: 'rocket', componente: 'produccion/tecnica-innovacion' }
    ]
  }

  ngOnInit(): void {
  }

  goToComponent(componente) {
    this.router.navigate([componente])
  }

  goToBackComponent() {
    this.router.navigate(['home']);
  }
}
