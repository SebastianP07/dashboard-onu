import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EditarProduccionAcademicaRoutingModule } from './editar-produccion-academica-routing.module';
import { EditarProduccionComponentAcademica } from './editar-produccion-academica.component';
import { GlobalComponent } from 'src/app/dashboard/global-components/global-components';


@NgModule({
  declarations: [EditarProduccionComponentAcademica],
  imports: [
    CommonModule,
    EditarProduccionAcademicaRoutingModule,
    GlobalComponent
  ]
})
export class EditarProduccionAcademicaModule { }
