import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditarProduccionComponentAcademica } from './editar-produccion-academica.component';

xdescribe('EditarProduccionComponent', () => {
  let component: EditarProduccionComponentAcademica;
  let fixture: ComponentFixture<EditarProduccionComponentAcademica>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditarProduccionComponentAcademica ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditarProduccionComponentAcademica);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
