import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EditarProduccionComponentAcademica } from './editar-produccion-academica.component';


const routes: Routes = [
  {path: '', component: EditarProduccionComponentAcademica}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EditarProduccionAcademicaRoutingModule { }
