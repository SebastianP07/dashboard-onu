import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CrearProduccionComponentAcademica } from './crear-produccion-academica.component';

xdescribe('CrearProduccionComponent', () => {
  let component: CrearProduccionComponentAcademica;
  let fixture: ComponentFixture<CrearProduccionComponentAcademica>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CrearProduccionComponentAcademica ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CrearProduccionComponentAcademica);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
