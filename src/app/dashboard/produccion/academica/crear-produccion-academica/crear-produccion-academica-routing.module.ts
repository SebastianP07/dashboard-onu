import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CrearProduccionComponentAcademica } from './crear-produccion-academica.component';


const routes: Routes = [
  {path: '', component: CrearProduccionComponentAcademica}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CrearProduccionAcademicaRoutingModule { }
