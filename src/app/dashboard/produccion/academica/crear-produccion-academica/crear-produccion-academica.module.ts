import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CrearProduccionAcademicaRoutingModule } from './crear-produccion-academica-routing.module';
import { CrearProduccionComponentAcademica } from './crear-produccion-academica.component';
import { GlobalComponent } from 'src/app/dashboard/global-components/global-components';
import { ReactiveFormsModule } from '@angular/forms';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';


@NgModule({
  declarations: [CrearProduccionComponentAcademica],
  imports: [
    CommonModule,
    CrearProduccionAcademicaRoutingModule,
    ReactiveFormsModule,
    GlobalComponent,
    BsDatepickerModule.forRoot()
  ]
})
export class CrearProduccionAcademicaModule { }
