import { Component, OnInit } from '@angular/core';
import {
  FormGroup,
  FormBuilder,
  FormControl,
  Validators,
} from '@angular/forms';
import { InvestigadoresData } from '../../../../core/models/endpoints';
import { Router } from '@angular/router';
import { UserServices } from 'src/app/core/services/user.service';
import { GrupoServices } from 'src/app/core/services/grupo.services';
import { DatePipe } from '@angular/common';
import { defineLocale } from 'ngx-bootstrap/chronos';
import { esLocale } from 'ngx-bootstrap/locale';
import { ProduccionServices } from 'src/app/core/services/produccion.service';
import { ModalComponent } from 'src/app/core/modal/modal.component';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';

@Component({
  selector: 'app-crear-produccion-academica',
  templateUrl: './crear-produccion-academica.component.html',
  styleUrls: ['./crear-produccion-academica.component.css'],
  providers: [UserServices, GrupoServices, ProduccionServices],
})
export class CrearProduccionComponentAcademica implements OnInit {
  programaForm: FormGroup;
  diplomadoForm: FormGroup;
  cursoForm: FormGroup;
  investigadores: any[];
  seleccionMultiple: any;
  gruposRegistrados: any;
  programasAcademicos: any[];
  instituciones: any;
  datePipe = new DatePipe('en-US');
  bsModalRef: BsModalRef;
  areaConocimiento: any;
  tipoPrograma: any;
  
  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private userServices: UserServices,
    private grupoServices: GrupoServices,
    private produccionServices: ProduccionServices,
    private modalService: BsModalService,
  ) {
    defineLocale('es', esLocale);

    this.seleccionMultiple = {
      programaAcademico: true,
      diplomado: false,
      curso: false,
    };

    this.investigadores = InvestigadoresData;

    this.programaForm = this.formBuilder.group({
      idGrupoInvestigacion: new FormControl(
        // Valor por defecto
        '',
        // Conjunto de validaciones
        Validators.compose([Validators.required])
      ),
      denominacion: new FormControl(
        // Valor por defecto
        '',
        // Conjunto de validaciones
        Validators.compose([Validators.required])
      ),
      registroCalificado: new FormControl(
        // Valor por defecto
        '',
        // Conjunto de validaciones
        Validators.compose([Validators.required, Validators.minLength(5)])
      ),
      registroAcreditacion: new FormControl(
        // Valor por defecto
        '',
        // Conjunto de validaciones
        Validators.compose([Validators.required, Validators.minLength(5)])
      ),
      fechaPrimerCorte: new FormControl(
        // Valor por defecto
        '',
        // Conjunto de validaciones
        Validators.compose([Validators.required])
      ),
      idTipoPrograma: new FormControl(
        // Valor por defecto
        '',
        // Conjunto de validaciones
        Validators.compose([Validators.required])
      ),
      nombre: new FormControl(
        // Valor por defecto
        '',
        // Conjunto de validaciones
        Validators.compose([Validators.required])
      ),
      idInstitucion: new FormControl(
        // Valor por defecto
        '',
        // Conjunto de validaciones
        Validators.compose([Validators.required])
      ),
    });

    this.diplomadoForm = this.formBuilder.group({
      idGrupoInvestigacion: new FormControl(
        // Valor por defecto
        '',
        // Conjunto de validaciones
        Validators.compose([Validators.required])
      ),
      idAreaConocimiento: new FormControl(
        // Valor por defecto
        '',
        // Conjunto de validaciones
        Validators.compose([Validators.required])
      ),
      descripcion: new FormControl(
        // Valor por defecto
        '',
        // Conjunto de validaciones
        Validators.compose([Validators.required])
      ),
      fechaPrimerCorte: new FormControl(
        // Valor por defecto
        '',
        // Conjunto de validaciones
        Validators.compose([Validators.required])
      ),
      idInstitucion: new FormControl(
        // Valor por defecto
        '',
        // Conjunto de validaciones
        Validators.compose([Validators.required])
      ),
      nombre: new FormControl(
        // Valor por defecto
        '',
        // Conjunto de validaciones
        Validators.compose([Validators.required])
      ),
      denominacion: new FormControl(
        // Valor por defecto
        '',
        // Conjunto de validaciones
        Validators.compose([Validators.required])
      ),
    });

    this.cursoForm = this.formBuilder.group({
      idProgramaAcademico: new FormControl(
        // Valor por defecto
        '',
        // Conjunto de validaciones
        Validators.compose([Validators.required])
      ),
      idAreaConocimiento: new FormControl(
        // Valor por defecto
        '',
        // Conjunto de validaciones
        Validators.compose([Validators.required])
      ),
      nombre: new FormControl(
        // Valor por defecto
        '',
        // Conjunto de validaciones
        Validators.compose([Validators.required])
      ),
      denominacion: new FormControl(
        // Valor por defecto
        '',
        // Conjunto de validaciones
        Validators.compose([Validators.required])
      ),
    });
  }

  ngOnInit() {
    this.obtenerProgramasAcademicos();
    this.instituciones = JSON.parse(localStorage.getItem("Instituciones"));
    this.areaConocimiento = JSON.parse(localStorage.getItem("AreaConocimiento"));
    this.tipoPrograma = JSON.parse(localStorage.getItem("TipoPrograma"));
  }

  submitProduccionForm(produccionForm) {
    console.log(produccionForm);
  }

  goToBackComponent() {
    this.router.navigate(['produccion/academica']);
  }

  obtenerProgramasAcademicos() {
    this.userServices.obtenerProgramasAcademicos().subscribe(
      (response) => {
        console.log('response obtenerProgramasAcademicos => ', response);
        if (response.codigo == 1) {
          this.programasAcademicos =
            response.objetoRespuestaArrayProgramaAcademico;
            this.obtenerGruposRegistrados();
        } else {
          console.error('error obtenerProgramasAcademicos => ', response);
          //this.router.navigate(['produccion/academica']);
        }
      },
      (error) => {
        console.error('error obtenerProgramasAcademicos => ', error);
        //this.router.navigate(['produccion/academica']);
      }
    );
  }

  obtenerGruposRegistrados() {
    this.grupoServices.verGruposRegistrados().subscribe(
      (response) => {
        console.log('response obtenerGruposRegistrados => ', response);
        if (response.codigo == 1) {
          this.gruposRegistrados =
            response.objetoRespuestaArrayGrupoInvestigacion;
        } else {
          console.error('error obtenerGruposRegistrados => ', response);
          //this.router.navigate(['produccion/academica']);
        }
      },
      (error) => {
        console.error('error obtenerGruposRegistrados => ', error);
        //this.router.navigate(['produccion/academica']);
      }
    );
  }

  cambioFormulario(nuevoValor) {
    switch (nuevoValor) {
      case 'programaAcademico':
        this.seleccionMultiple = {
          programaAcademico: true,
          diplomado: false,
          curso: false,
        };
        break;
      case 'diplomado':
        this.seleccionMultiple = {
          programaAcademico: false,
          diplomado: true,
          curso: false,
        };
        break;
      case 'curso':
        this.seleccionMultiple = {
          programaAcademico: false,
          diplomado: false,
          curso: true,
        };
        break;
    }
  }

  openModalWithComponent(titulo, mensaje) {
    const initialState = {
      title: `${titulo}`,
      list: `${mensaje}`,
    };
    this.bsModalRef = this.modalService.show(ModalComponent, { initialState });
    this.bsModalRef.content.closeBtnName = 'Cerrar';
  }

  submitProgramaForm(programaForm){
    programaForm.fechaPrimerCorte = this.datePipe.transform(programaForm.fechaPrimerCorte, 'dd/MM/yyyy');
    programaForm.registroAcreditacion = programaForm.registroAcreditacion.toString();
    programaForm.registroCalificado = programaForm.registroCalificado.toString();

    this.produccionServices.registrarProgramaAcademico(programaForm).subscribe(
      response => {
        console.log("response registrarProgramaAcademico => ", response);
        if (response.codigo == 1) {
          this.programaForm.reset();
          this.openModalWithComponent('Creación programa academico', response.descripcion);
        } else {
          this.programaForm.reset();
          this.openModalWithComponent('Error al crear el programa academico', response.descripcion);
          console.error("error registrarProgramaAcademico => ", response.descripcionError);
        }
      },
      error => {
        this.programaForm.reset();
        console.error("error registrarProgramaAcademico => ", error);
      }
    );
  }

  submitDiplomadoForm(diplomadoForm){
    diplomadoForm.fechaPrimerCorte = this.datePipe.transform(diplomadoForm.fechaPrimerCorte, 'dd/MM/yyyy');

    this.produccionServices.registrarDiplomado(diplomadoForm).subscribe(
      response => {
        console.log("response registrarDiplomado => ", response);
        if (response.codigo == 1) {
          this.diplomadoForm.reset();
          this.openModalWithComponent('Creación diplomado', response.descripcion);
        } else {
          this.diplomadoForm.reset();
          this.openModalWithComponent('Error al crear el diplomado', response.descripcion);
          console.error("error registrarDiplomado => ", response.descripcionError);
        }
      },
      error => {
        this.diplomadoForm.reset();
        console.error("error registrarDiplomado => ", error);
      }
    );
  }

  submitCursoForm(cursoForm){
    this.produccionServices.registrarCursoProgramaAcademico(cursoForm).subscribe(
      response => {
        console.log("response registrarCursoProgramaAcademico => ", response);
        if (response.codigo == 1) {
          this.cursoForm.reset();
          this.openModalWithComponent('Creación curso del programa académico', response.descripcion);
        } else {
          this.cursoForm.reset();
          this.openModalWithComponent('Error al crear el curso del programa académico', response.descripcion);
          console.error("error registrarCursoProgramaAcademico => ", response.descripcionError);
        }
      },
      error => {
        this.cursoForm.reset();
        console.error("error registrarCursoProgramaAcademico => ", error);
      }
    );
    
  }
}
