import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AcademicaComponent } from './academica.component';


const routes: Routes = [
  {
    path: '',
    component: AcademicaComponent
  },
  {
    path: 'crear-produccion-academica',
    loadChildren: () => import('./crear-produccion-academica/crear-produccion-academica.module').then(m => m.CrearProduccionAcademicaModule)
  },
  {
    path: 'editar-produccion-academica',
    loadChildren: () => import('./editar-produccion-academica/editar-produccion-academica.module').then(m => m.EditarProduccionAcademicaModule)
  },
  {
    path: 'eliminar-produccion-academica',
    loadChildren: () => import('./eliminar-produccion-academica/eliminar-produccion-academica.module').then(m => m.EliminarProduccionAcademicaModule)
  },
  {
    path: 'ver-produccion-academica',
    loadChildren: () => import('./ver-produccion-academica/ver-produccion-academica.module').then(m => m.VerProduccionAcademicaModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AcademicaRoutingModule { }
