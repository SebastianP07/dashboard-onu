import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { VerProduccionAcademicaRoutingModule } from './ver-produccion-academica-routing.module';
import { VerProduccionComponentAcademica } from './ver-produccion-academica.component';
import { GlobalComponent } from 'src/app/dashboard/global-components/global-components';


@NgModule({
  declarations: [VerProduccionComponentAcademica],
  imports: [
    CommonModule,
    VerProduccionAcademicaRoutingModule,
    GlobalComponent
  ]
})
export class VerProduccionAcademicaModule { }
