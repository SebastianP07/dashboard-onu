import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { VerProduccionComponentAcademica } from './ver-produccion-academica.component';


const routes: Routes = [
  {path:'', component: VerProduccionComponentAcademica}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class VerProduccionAcademicaRoutingModule { }
