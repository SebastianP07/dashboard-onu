import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VerProduccionComponentAcademica } from './ver-produccion-academica.component';

xdescribe('VerProduccionComponent', () => {
  let component: VerProduccionComponentAcademica;
  let fixture: ComponentFixture<VerProduccionComponentAcademica>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VerProduccionComponentAcademica ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VerProduccionComponentAcademica);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
