import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ProduccionServices } from 'src/app/core/services/produccion.service';

@Component({
  selector: 'app-ver-produccion-academica',
  templateUrl: './ver-produccion-academica.component.html',
  styleUrls: ['./ver-produccion-academica.component.css'],
  providers: [ProduccionServices]
})
export class VerProduccionComponentAcademica implements OnInit {
  
  dataTableOptions: DataTables.Settings = {};
  dataTable: any;
  seleccionMultiple: any;
  dataModal: any;
  produccionAcademicaAlmacenada: any;

  constructor(
    private router: Router,
    private produccionServices: ProduccionServices
  ) {
    this.seleccionMultiple = {
      programaAcademico: true,
      diplomado: false,
      curso: false,
    };
  }

  ngOnInit(): void {
    this.dataTableOptions = {
      pagingType: 'full_numbers',
      pageLength: 5,
      processing: true,
    };

    this.obtenerProduccionAcademicaAlmacenada();
  }

  goToBackComponent() {
    this.router.navigate(['produccion/academica']);
  }

  cambioFormulario(nuevoValor) {
    switch (nuevoValor) {
      case 'programaAcademico':
        this.seleccionMultiple = {
          programaAcademico: true,
          diplomado: false,
          curso: false,
        };
        break;
      case 'diplomado':
        this.seleccionMultiple = {
          programaAcademico: false,
          diplomado: true,
          curso: false,
        };
        break;
      case 'curso':
        this.seleccionMultiple = {
          programaAcademico: false,
          diplomado: false,
          curso: true,
        };
        break;
    }
  }

  obtenerProduccionAcademicaAlmacenada(){
    this.produccionServices.verProduccionAcademicaAlmacenada().subscribe(
      (response) => {
        console.log('response verProduccionAcademicaAlmacenada => ', response);
        if (response.codigo == 1) {
          this.produccionAcademicaAlmacenada = response;
        } else {
          console.error('error verProduccionAcademicaAlmacenada => ', response);
          alert('Error al obtener la produccion academica almacenada');
          this.router.navigate(['produccion/academica']);
        }
      },
      (error) => {
        console.error('error verProduccionAcademicaAlmacenada => ', error);
        alert('Error al obtener la produccion academica almacenada');
        this.router.navigate(['produccion/academica']);
      }
    );
  }
}
