import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EliminarProduccionComponentAcademica } from './eliminar-produccion-academica.component';

xdescribe('EliminarProduccionComponent', () => {
  let component: EliminarProduccionComponentAcademica;
  let fixture: ComponentFixture<EliminarProduccionComponentAcademica>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EliminarProduccionComponentAcademica ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EliminarProduccionComponentAcademica);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
