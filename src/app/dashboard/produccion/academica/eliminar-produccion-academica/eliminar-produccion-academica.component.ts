import { Component, OnInit, TemplateRef } from '@angular/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { Router } from '@angular/router';
import { ProduccionServices } from 'src/app/core/services/produccion.service';

@Component({
  selector: 'app-eliminar-produccion-academica',
  templateUrl: './eliminar-produccion-academica.component.html',
  styleUrls: ['./eliminar-produccion-academica.component.css'],
  providers: [ProduccionServices]
})
export class EliminarProduccionComponentAcademica implements OnInit {
  
  dataTableOptions: DataTables.Settings = {};
  dataTable: any;
  modalRef: BsModalRef;
  seleccionMultiple: any;
  dataModal: any;
  produccionAcademicaAlmacenada: any;

  constructor(
    private modalService: BsModalService,
    private router: Router,
    private produccionServices: ProduccionServices
  ) {
    this.seleccionMultiple = {
      programaAcademico: true,
      diplomado: false,
      curso: false,
    };
  }

  ngOnInit(): void {
    this.dataTableOptions = {
      pagingType: 'full_numbers',
      pageLength: 5,
      processing: true,
    };

    this.obtenerProduccionAcademicaAlmacenada();
  }

  openModal(template: TemplateRef<any>, idRegistro) {
    this.dataModal = idRegistro;


    this.modalRef = this.modalService.show(template, {
      ariaDescribedby: 'my-modal-description',
      ariaLabelledBy: 'my-modal-title'
    });
  }

  actualizarRegistro(idRegistro){
    alert(idRegistro);
    this.modalRef.hide();
  }

  goToBackComponent() {
    this.router.navigate(['produccion/academica']);
  }

  cambioFormulario(nuevoValor) {
    switch (nuevoValor) {
      case 'programaAcademico':
        this.seleccionMultiple = {
          programaAcademico: true,
          diplomado: false,
          curso: false,
        };
        break;
      case 'diplomado':
        this.seleccionMultiple = {
          programaAcademico: false,
          diplomado: true,
          curso: false,
        };
        break;
      case 'curso':
        this.seleccionMultiple = {
          programaAcademico: false,
          diplomado: false,
          curso: true,
        };
        break;
    }
  }

  obtenerProduccionAcademicaAlmacenada(){
    this.produccionServices.verProduccionAcademicaAlmacenada().subscribe(
      (response) => {
        console.log('response verProduccionAcademicaAlmacenada => ', response);
        if (response.codigo == 1) {
          this.produccionAcademicaAlmacenada = response;
        } else {
          console.error('error verProduccionAcademicaAlmacenada => ', response);
          alert('Error al obtener la produccion academica almacenada');
          this.router.navigate(['produccion/academica']);
        }
      },
      (error) => {
        console.error('error verProduccionAcademicaAlmacenada => ', error);
        alert('Error al obtener la produccion academica almacenada');
        this.router.navigate(['produccion/academica']);
      }
    );
  }
}
