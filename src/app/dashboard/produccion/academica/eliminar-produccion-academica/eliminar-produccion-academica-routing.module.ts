import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EliminarProduccionComponentAcademica } from './eliminar-produccion-academica.component';


const routes: Routes = [
  {path:'', component: EliminarProduccionComponentAcademica}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EliminarProduccionAcademicaRoutingModule { }
