import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EliminarProduccionAcademicaRoutingModule } from './eliminar-produccion-academica-routing.module';
import { EliminarProduccionComponentAcademica } from './eliminar-produccion-academica.component';
import { GlobalComponent } from 'src/app/dashboard/global-components/global-components';


@NgModule({
  declarations: [EliminarProduccionComponentAcademica],
  imports: [
    CommonModule,
    EliminarProduccionAcademicaRoutingModule,
    GlobalComponent
  ]
})
export class EliminarProduccionAcademicaModule { }
