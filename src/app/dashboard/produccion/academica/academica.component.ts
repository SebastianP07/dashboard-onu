import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-academica',
  templateUrl: './academica.component.html',
  styleUrls: ['./academica.component.css'],
})
export class AcademicaComponent implements OnInit {
  cardArray: Array<any>;

  constructor(private router: Router) {
    this.cardArray = [
      {
        texto: 'Crear producción',
        icono: 'plus-circle',
        componente: 'produccion/academica/crear-produccion-academica',
      },
      {
        texto: 'Editar producción',
        icono: 'edit',
        componente: 'produccion/academica/editar-produccion-academica',
      },
      {
        texto: 'Eliminar producción',
        icono: 'trash-alt',
        componente: 'produccion/academica/eliminar-produccion-academica',
      },
      {
        texto: 'Ver producción',
        icono: 'list',
        componente: 'produccion/academica/ver-produccion-academica',
      },
    ];
  }

  ngOnInit(): void {}

  goToComponent(componente) {
    this.router.navigate([componente]);
  }

  goToBackComponent() {
    this.router.navigate(['produccion']);
  }
}
