import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AcademicaRoutingModule } from './academica-routing.module';
import { AcademicaComponent } from './academica.component';


@NgModule({
  declarations: [AcademicaComponent],
  imports: [
    CommonModule,
    AcademicaRoutingModule
  ]
})
export class AcademicaModule { }
