import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-tecnica-innovacion',
  templateUrl: './tecnica-innovacion.component.html',
  styleUrls: ['./tecnica-innovacion.component.css']
})
export class TecnicaInnovacionComponent implements OnInit {
  cardArray: Array<any>;

  constructor(
    private router: Router
  ) {
    this.cardArray = [
      { texto: 'Crear producción', icono: 'plus-circle', componente: 'produccion/tecnica-innovacion/crear-produccion-tecnica-innovacion' },
      { texto: 'Editar producción', icono: 'edit', componente: 'produccion/tecnica-innovacion/editar-produccion-tecnica-innovacion' },
      { texto: 'Eliminar producción', icono: 'trash-alt', componente: 'produccion/tecnica-innovacion/eliminar-produccion-tecnica-innovacion' },
      { texto: 'Ver producción', icono: 'list', componente: 'produccion/tecnica-innovacion/ver-produccion-tecnica-innovacion' }
    ]
  }

  ngOnInit(): void {
  }

  goToComponent(componente) {
    this.router.navigate([componente])
  }

}
