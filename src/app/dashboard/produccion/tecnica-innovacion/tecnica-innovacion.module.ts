import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TecnicaInnovacionRoutingModule } from './tecnica-innovacion-routing.module';
import { TecnicaInnovacionComponent } from './tecnica-innovacion.component';


@NgModule({
  declarations: [TecnicaInnovacionComponent],
  imports: [
    CommonModule,
    TecnicaInnovacionRoutingModule
  ]
})
export class TecnicaInnovacionModule { }
