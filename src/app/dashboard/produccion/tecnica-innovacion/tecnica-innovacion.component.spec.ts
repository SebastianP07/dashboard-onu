import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TecnicaInnovacionComponent } from './tecnica-innovacion.component';

xdescribe('TecnicaInnovacionComponent', () => {
  let component: TecnicaInnovacionComponent;
  let fixture: ComponentFixture<TecnicaInnovacionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TecnicaInnovacionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TecnicaInnovacionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
  
});
