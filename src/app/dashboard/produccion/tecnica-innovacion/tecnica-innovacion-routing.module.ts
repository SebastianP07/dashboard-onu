import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TecnicaInnovacionComponent } from './tecnica-innovacion.component';


const routes: Routes = [
  {
    path: '',
    component: TecnicaInnovacionComponent
  },
  {
    path: 'crear-produccion-tecnica-innovacion',
    loadChildren: () => import('./crear-produccion-tecnica-innovacion/crear-produccion-tecnica-innovacion.module').then(m => m.CrearProduccionTecnicaInnovacionModule)
  },
  {
    path: 'editar-produccion-tecnica-innovacion',
    loadChildren: () => import('./editar-produccion-tecnica-innovacion/editar-produccion-tecnica-innovacion.module').then(m => m.EditarProduccionTecnicaInnovacionModule)
  },
  {
    path: 'eliminar-produccion-tecnica-innovacion',
    loadChildren: () => import('./eliminar-produccion-tecnica-innovacion/eliminar-produccion-tecnica-innovacion.module').then(m => m.EliminarProduccionTecnicaInnovacionModule)
  },
  {
    path: 'ver-produccion-tecnica-innovacion',
    loadChildren: () => import('./ver-produccion-tecnica-innovacion/ver-produccion-tecnica-innovacion.module').then(m => m.VerProduccionTecnicaInnovacionModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TecnicaInnovacionRoutingModule { }
