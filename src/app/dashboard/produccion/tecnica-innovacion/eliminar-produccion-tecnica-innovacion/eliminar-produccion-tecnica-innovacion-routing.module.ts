import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EliminarProduccionTecnicaInnovacionComponent } from './eliminar-produccion-tecnica-innovacion.component';


const routes: Routes = [
  {path:'', component: EliminarProduccionTecnicaInnovacionComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EliminarProduccionTecnicaInnovacionRoutingModule { }
