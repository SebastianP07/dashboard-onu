import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EliminarProduccionTecnicaInnovacionComponent } from './eliminar-produccion-tecnica-innovacion.component';

xdescribe('EliminarProduccionTecnicaInnovacionComponent', () => {
  let component: EliminarProduccionTecnicaInnovacionComponent;
  let fixture: ComponentFixture<EliminarProduccionTecnicaInnovacionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EliminarProduccionTecnicaInnovacionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EliminarProduccionTecnicaInnovacionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
