import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EliminarProduccionTecnicaInnovacionRoutingModule } from './eliminar-produccion-tecnica-innovacion-routing.module';
import { EliminarProduccionTecnicaInnovacionComponent } from './eliminar-produccion-tecnica-innovacion.component';


@NgModule({
  declarations: [EliminarProduccionTecnicaInnovacionComponent],
  imports: [
    CommonModule,
    EliminarProduccionTecnicaInnovacionRoutingModule
  ]
})
export class EliminarProduccionTecnicaInnovacionModule { }
