import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CrearProduccionTecnicaInnovacionRoutingModule } from './crear-produccion-tecnica-innovacion-routing.module';
import { CrearProduccionTecnicaInnovacionComponent } from './crear-produccion-tecnica-innovacion.component';


@NgModule({
  declarations: [CrearProduccionTecnicaInnovacionComponent],
  imports: [
    CommonModule,
    CrearProduccionTecnicaInnovacionRoutingModule
  ]
})
export class CrearProduccionTecnicaInnovacionModule { }
