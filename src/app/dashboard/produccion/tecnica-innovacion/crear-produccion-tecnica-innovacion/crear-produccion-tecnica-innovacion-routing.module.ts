import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CrearProduccionTecnicaInnovacionComponent } from './crear-produccion-tecnica-innovacion.component';


const routes: Routes = [
  { path: '', component: CrearProduccionTecnicaInnovacionComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CrearProduccionTecnicaInnovacionRoutingModule { }
