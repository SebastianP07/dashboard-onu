import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CrearProduccionTecnicaInnovacionComponent } from './crear-produccion-tecnica-innovacion.component';

xdescribe('CrearProduccionTecnicaInnovacionComponent', () => {
  let component: CrearProduccionTecnicaInnovacionComponent;
  let fixture: ComponentFixture<CrearProduccionTecnicaInnovacionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CrearProduccionTecnicaInnovacionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CrearProduccionTecnicaInnovacionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
