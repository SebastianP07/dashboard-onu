import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EditarProduccionTecnicaInnovacionRoutingModule } from './editar-produccion-tecnica-innovacion-routing.module';
import { EditarProduccionTecnicaInnovacionComponent } from './editar-produccion-tecnica-innovacion.component';


@NgModule({
  declarations: [EditarProduccionTecnicaInnovacionComponent],
  imports: [
    CommonModule,
    EditarProduccionTecnicaInnovacionRoutingModule
  ]
})
export class EditarProduccionTecnicaInnovacionModule { }
