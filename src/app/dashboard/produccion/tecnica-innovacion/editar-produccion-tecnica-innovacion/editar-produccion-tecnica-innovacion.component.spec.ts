import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditarProduccionTecnicaInnovacionComponent } from './editar-produccion-tecnica-innovacion.component';

xdescribe('EditarProduccionTecnicaInnovacionComponent', () => {
  let component: EditarProduccionTecnicaInnovacionComponent;
  let fixture: ComponentFixture<EditarProduccionTecnicaInnovacionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditarProduccionTecnicaInnovacionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditarProduccionTecnicaInnovacionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
