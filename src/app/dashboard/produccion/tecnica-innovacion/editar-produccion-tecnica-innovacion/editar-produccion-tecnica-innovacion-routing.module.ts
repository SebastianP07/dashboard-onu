import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EditarProduccionTecnicaInnovacionComponent } from './editar-produccion-tecnica-innovacion.component';


const routes: Routes = [
  {path:'', component: EditarProduccionTecnicaInnovacionComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EditarProduccionTecnicaInnovacionRoutingModule { }
