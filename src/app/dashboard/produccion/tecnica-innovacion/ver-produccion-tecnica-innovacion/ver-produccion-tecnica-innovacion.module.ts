import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { VerProduccionTecnicaInnovacionRoutingModule } from './ver-produccion-tecnica-innovacion-routing.module';
import { VerProduccionTecnicaInnovacionComponent } from './ver-produccion-tecnica-innovacion.component';


@NgModule({
  declarations: [VerProduccionTecnicaInnovacionComponent],
  imports: [
    CommonModule,
    VerProduccionTecnicaInnovacionRoutingModule
  ]
})
export class VerProduccionTecnicaInnovacionModule { }
