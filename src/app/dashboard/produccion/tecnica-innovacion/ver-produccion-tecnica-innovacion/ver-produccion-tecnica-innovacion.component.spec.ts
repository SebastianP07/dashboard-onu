import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VerProduccionTecnicaInnovacionComponent } from './ver-produccion-tecnica-innovacion.component';

xdescribe('VerProduccionTecnicaInnovacionComponent', () => {
  let component: VerProduccionTecnicaInnovacionComponent;
  let fixture: ComponentFixture<VerProduccionTecnicaInnovacionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VerProduccionTecnicaInnovacionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VerProduccionTecnicaInnovacionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
