import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { VerProduccionTecnicaInnovacionComponent } from './ver-produccion-tecnica-innovacion.component';


const routes: Routes = [
  {path:'', component: VerProduccionTecnicaInnovacionComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class VerProduccionTecnicaInnovacionRoutingModule { }
