import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProduccionComponent } from './produccion.component';


const routes: Routes = [
  {
    path: '',
    component: ProduccionComponent
  },
  {
    path: 'academica',
    loadChildren: () => import('./academica/academica.module').then(m => m.AcademicaModule)
  },
  {
    path: 'bibliografica',
    loadChildren: () => import('./bibliografica/bibliografica.module').then(m => m.BibliograficaModule)
  },
  {
    path: 'tecnica-innovacion',
    loadChildren: () => import('./tecnica-innovacion/tecnica-innovacion.module').then(m => m.TecnicaInnovacionModule)
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProduccionRoutingModule { }
