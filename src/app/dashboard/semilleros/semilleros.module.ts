import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SemillerosRoutingModule } from './semilleros-routing.module';
import { SemillerosComponent } from './semilleros.component';

@NgModule({
  imports: [
    CommonModule,
    SemillerosRoutingModule,
  ],
  declarations: [
    SemillerosComponent
  ],
})
export class SemillerosModule { }
