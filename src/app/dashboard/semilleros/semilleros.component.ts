import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-semilleros',
  templateUrl: './semilleros.component.html',
  styleUrls: ['./semilleros.component.css'],
})
export class SemillerosComponent implements OnInit {
  cardArray: Array<any>;

  constructor(private router: Router) {
    this.cardArray = [
      {
        texto: 'Crear semillero',
        icono: 'plus-circle',
        componente: 'semilleros/crear-semillero',
      },
      {
        texto: 'Editar semillero',
        icono: 'edit',
        componente: 'semilleros/editar-semillero',
      },
      {
        texto: 'Eliminar semillero',
        icono: 'trash-alt',
        componente: 'semilleros/eliminar-semillero',
      },
      {
        texto: 'Ver semilleros',
        icono: 'list',
        componente: 'semilleros/ver-semillero',
      },
    ];
  }

  ngOnInit(): void {}

  goToComponent(componente) {
    this.router.navigate([componente]);
  }

  goToBackComponent() {
    this.router.navigate(['home']);
  }
}
