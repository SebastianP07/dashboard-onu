import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SemillerosComponent } from './semilleros.component';
import { CrearSemilleroComponent } from './crear-semillero/crear-semillero.component';


const routes: Routes = [
  {
    path: '',
    component: SemillerosComponent
  },
  {
    path: 'crear-semillero',
    loadChildren: () => import('./crear-semillero/crear-semillero.module').then(m => m.CrearSemilleroModule)
  },
  {
    path: 'editar-semillero',
    loadChildren: () => import('./editar-semillero/editar-semillero.module').then(m => m.EditarSemilleroModule)
  },
  {
    path: 'eliminar-semillero',
    loadChildren: () => import('./eliminar-semillero/eliminar-semillero.module').then(m => m.EliminarSemilleroModule)
  },
  {
    path: 'ver-semillero',
    loadChildren: () => import('./ver-semillero/ver-semillero.module').then(m => m.VerSemilleroModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SemillerosRoutingModule { }
