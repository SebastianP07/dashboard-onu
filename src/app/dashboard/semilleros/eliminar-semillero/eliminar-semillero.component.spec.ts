import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EliminarSemilleroComponent } from './eliminar-semillero.component';

xdescribe('EliminarSemilleroComponent', () => {
  let component: EliminarSemilleroComponent;
  let fixture: ComponentFixture<EliminarSemilleroComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EliminarSemilleroComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EliminarSemilleroComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
