import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EliminarSemilleroRoutingModule } from './eliminar-semillero-routing.module';
import { EliminarSemilleroComponent } from './eliminar-semillero.component';
import { GlobalComponent } from '../../global-components/global-components';


@NgModule({
  declarations: [EliminarSemilleroComponent],
  imports: [
    CommonModule,
    EliminarSemilleroRoutingModule,
    GlobalComponent
  ]
})
export class EliminarSemilleroModule { }
