import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EliminarSemilleroComponent } from './eliminar-semillero.component';


const routes: Routes = [
  { path: '', component: EliminarSemilleroComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EliminarSemilleroRoutingModule { }
