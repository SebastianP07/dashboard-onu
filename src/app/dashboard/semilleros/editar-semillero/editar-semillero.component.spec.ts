import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditarSemilleroComponent } from './editar-semillero.component';

xdescribe('EditarSemilleroComponent', () => {
  let component: EditarSemilleroComponent;
  let fixture: ComponentFixture<EditarSemilleroComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditarSemilleroComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditarSemilleroComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
