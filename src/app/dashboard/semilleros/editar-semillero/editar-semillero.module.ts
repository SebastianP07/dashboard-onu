import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EditarSemilleroRoutingModule } from './editar-semillero-routing.module';
import { EditarSemilleroComponent } from './editar-semillero.component';
import { GlobalComponent } from '../../global-components/global-components';


@NgModule({
  declarations: [EditarSemilleroComponent],
  imports: [
    CommonModule,
    EditarSemilleroRoutingModule,
    GlobalComponent
  ]
})
export class EditarSemilleroModule { }
