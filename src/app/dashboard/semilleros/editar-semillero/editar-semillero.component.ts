import { Component, OnInit, TemplateRef } from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { Router } from '@angular/router';

@Component({
  selector: 'app-editar-semillero',
  templateUrl: './editar-semillero.component.html',
  styleUrls: ['./editar-semillero.component.css'],
})
export class EditarSemilleroComponent implements OnInit {
  dataTableOptions: DataTables.Settings = {};
  dataTable: any;
  modalRef: BsModalRef;

  dataModal: any;

  constructor(private modalService: BsModalService, private router: Router) {
    this.dataTable = [
      { id: '1', fistName: 'Sebastian', lastName: 'Pinto' },
      { id: '2', fistName: 'Sebastian', lastName: 'Pinto' },
      { id: '3', fistName: 'Sebastian', lastName: 'Pinto' },
      { id: '4', fistName: 'Sebastian', lastName: 'Pinto' },
      { id: '5', fistName: 'Sebastian', lastName: 'Pinto' },
      { id: '6', fistName: 'Sebastian', lastName: 'Pinto' },
      { id: '7', fistName: 'Sebastian', lastName: 'Pinto' },
      { id: '8', fistName: 'Sebastian', lastName: 'Pinto' },
      { id: '9', fistName: 'Sebastian', lastName: 'Pinto' },
      { id: '10', fistName: 'Sebastian', lastName: 'Pinto' },
    ];
  }

  ngOnInit(): void {
    this.dataTableOptions = {
      pagingType: 'full_numbers',
      pageLength: 5,
      processing: true,
    };
  }

  openModal(template: TemplateRef<any>, idRegistro) {
    this.dataModal = idRegistro;

    this.modalRef = this.modalService.show(template, {
      ariaDescribedby: 'my-modal-description',
      ariaLabelledBy: 'my-modal-title',
    });
  }

  actualizarRegistro(idRegistro) {
    alert(idRegistro);
    this.modalRef.hide();
  }

  goToBackComponent() {
    this.router.navigate(['semilleros']);
  }
}
