import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EditarSemilleroComponent } from './editar-semillero.component';


const routes: Routes = [
  { path: '', component: EditarSemilleroComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EditarSemilleroRoutingModule { }
