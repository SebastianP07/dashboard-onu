import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CrearSemilleroRoutingModule } from './crear-semillero-routing.module';
import { CrearSemilleroComponent } from './crear-semillero.component';
import { GlobalComponent } from '../../global-components/global-components';
import { ReactiveFormsModule } from '@angular/forms';


@NgModule({
  declarations: [CrearSemilleroComponent],
  imports: [
    CommonModule,
    CrearSemilleroRoutingModule,
    ReactiveFormsModule,
    GlobalComponent
  ]
})
export class CrearSemilleroModule { }
