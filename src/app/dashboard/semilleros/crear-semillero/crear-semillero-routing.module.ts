import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CrearSemilleroComponent } from './crear-semillero.component';

const routes: Routes = [{ path: '', component: CrearSemilleroComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CrearSemilleroRoutingModule { }
