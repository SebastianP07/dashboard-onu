import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { InvestigadoresData } from 'src/app/core/models/endpoints';
import { Router } from '@angular/router';

@Component({
  selector: 'app-crear-semillero',
  templateUrl: './crear-semillero.component.html',
  styleUrls: ['./crear-semillero.component.css']
})
export class CrearSemilleroComponent implements OnInit {
  semilleroForm: FormGroup;
  investigadores: any[];

  constructor(private formBuilder: FormBuilder, private router: Router) {
    this.investigadores = InvestigadoresData;

    this.semilleroForm = this.formBuilder.group({
      idInvestigador: new FormControl(
        // Valor por defecto
        '',
        // Conjunto de validaciones
        Validators.compose([Validators.required])
      ),
      paginaWeb: new FormControl(
        // Valor por defecto
        '',
        // Conjunto de validaciones
        Validators.compose([Validators.required])
      ),
    });
  }

  ngOnInit(): void {}

  submitSemilleroForm(semilleroForm) {
    console.log(semilleroForm);
  }

  goToBackComponent() {
    this.router.navigate(['semilleros']);
  }

}
