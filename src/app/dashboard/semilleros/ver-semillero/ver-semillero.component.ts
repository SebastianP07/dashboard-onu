import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-ver-semillero',
  templateUrl: './ver-semillero.component.html',
  styleUrls: ['./ver-semillero.component.css'],
})
export class VerSemilleroComponent implements OnInit {
  dtOptions: DataTables.Settings = {};
  dataTable: any;

  constructor(private router: Router) {
    this.dataTable = [
      { id: '1', fistName: 'Sebastian', lastName: 'Pinto' },
      { id: '2', fistName: 'Sebastian', lastName: 'Pinto' },
      { id: '3', fistName: 'Sebastian', lastName: 'Pinto' },
      { id: '4', fistName: 'Sebastian', lastName: 'Pinto' },
      { id: '5', fistName: 'Sebastian', lastName: 'Pinto' },
      { id: '6', fistName: 'Sebastian', lastName: 'Pinto' },
      { id: '7', fistName: 'Sebastian', lastName: 'Pinto' },
      { id: '8', fistName: 'Sebastian', lastName: 'Pinto' },
      { id: '9', fistName: 'Sebastian', lastName: 'Pinto' },
      { id: '10', fistName: 'Sebastian', lastName: 'Pinto' },
    ];
  }

  ngOnInit(): void {
    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 5,
      processing: true,
    };
  }

  goToBackComponent() {
    this.router.navigate(['semilleros']);
  }
}
