import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VerSemilleroComponent } from './ver-semillero.component';

xdescribe('VerSemilleroComponent', () => {
  let component: VerSemilleroComponent;
  let fixture: ComponentFixture<VerSemilleroComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VerSemilleroComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VerSemilleroComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
