import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { VerSemilleroRoutingModule } from './ver-semillero-routing.module';
import { VerSemilleroComponent } from './ver-semillero.component';
import { GlobalComponent } from '../../global-components/global-components';


@NgModule({
  declarations: [VerSemilleroComponent],
  imports: [
    CommonModule,
    VerSemilleroRoutingModule,
    GlobalComponent
  ]
})
export class VerSemilleroModule { }
