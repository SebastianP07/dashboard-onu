import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { VerSemilleroComponent } from './ver-semillero.component';


const routes: Routes = [
  { path: '', component: VerSemilleroComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class VerSemilleroRoutingModule { }
