import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AsignarGruposRoutingModule } from './asignar-grupos-routing.module';
import { AsignarGruposComponent } from './asignar-grupos.component';
import { GlobalComponent } from '../global-components/global-components';


@NgModule({
  declarations: [AsignarGruposComponent],
  imports: [
    CommonModule,
    AsignarGruposRoutingModule,
    GlobalComponent
  ]
})
export class AsignarGruposModule { }
