import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AsignarGruposComponent } from './asignar-grupos.component';


const routes: Routes = [
  { path: '', component: AsignarGruposComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AsignarGruposRoutingModule { }
