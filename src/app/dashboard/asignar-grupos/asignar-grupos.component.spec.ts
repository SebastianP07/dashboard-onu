import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AsignarGruposComponent } from './asignar-grupos.component';

xdescribe('AsignarGruposComponent', () => {
  let component: AsignarGruposComponent;
  let fixture: ComponentFixture<AsignarGruposComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AsignarGruposComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AsignarGruposComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
