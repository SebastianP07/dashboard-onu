import { Component, OnInit } from '@angular/core';
import { SortablejsOptions } from 'ngx-sortablejs';
import { InvestigadoresData } from '../../core/models/endpoints';
import { Router } from '@angular/router';

@Component({
  selector: 'app-asignar-grupos',
  templateUrl: './asignar-grupos.component.html',
  styleUrls: ['./asignar-grupos.component.css'],
})
export class AsignarGruposComponent implements OnInit {
  investigadores: any;
  list1Options: SortablejsOptions = {
    group: {
      name: 'group2',
      put: ['group1', 'group2'],
    },
  };

  constructor(private router: Router) {
    this.investigadores = InvestigadoresData;
  }

  ngOnInit(): void {}

  grupos = [
    {
      nombre: 'a',
      lista: [
        { nombre: 'Nombre A lider', primerapellido: 'Apellido A'},
        { nombre: 'Nombre A', primerapellido: 'Apellido A'},
      ],
    },
    {
      nombre: 'b',
      lista: [
        { nombre: 'Nombre B lider', primerapellido: 'Apellido B'},
        { nombre: 'Nombre B', primerapellido: 'Apellido B'},
      ],
    },
    {
      nombre: 'c',
      lista: [
        { nombre: 'Nombre C lider', primerapellido: 'Apellido C', tipo: 'lider' },
        { nombre: 'Nombre C', primerapellido: 'Apellido C'},
      ],
    },
  ];

  ver() {
    console.log(this.grupos);
  }

  goToBackComponent() {
    this.router.navigate(['home']);
  }
}
