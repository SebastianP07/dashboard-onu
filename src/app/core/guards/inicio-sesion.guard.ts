import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class InicioSesionGuard implements CanActivate {

  constructor(
    private router: Router
  ){}

  async canActivate() {
    const inicioSesionUsuario = await localStorage.getItem('datos-usuario');
    if (inicioSesionUsuario) {
      return true;
    } else {
      this.router.navigateByUrl('/inicio-sesion');
    }
  }
  
}
