import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ModalComponent } from './modal.component';
import { GlobalComponent } from 'src/app/dashboard/global-components/global-components';


@NgModule({
  declarations: [ModalComponent],
  imports: [
    CommonModule,
    GlobalComponent
  ]
})
export class ModalModuleComponent { }
