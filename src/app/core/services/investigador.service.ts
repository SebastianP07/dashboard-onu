import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { GlobalServicesRequests } from './global.service';

@Injectable()
export class InvestigadorServices {
  constructor(private globalServicesRequests: GlobalServicesRequests) {}

  registrarHistorialInvestigador(dataGrupo): Observable<any> {
    return this.globalServicesRequests.post('crearHistorialInvestigador/', dataGrupo);
  }
  
  registrarHistorialConferencia(dataGrupo): Observable<any> {
    return this.globalServicesRequests.post('crearHistorialConferencias/', dataGrupo);
  }

  obtenerMiemborsGrupo(): Observable<any> {
    return this.globalServicesRequests.get('verMiembrosGrupo/');
  }

  verHistorialCompletoInvestigadores(): Observable<any> {
    return this.globalServicesRequests.get('verHistorialCompletoInvestigadores/');
  }

}
