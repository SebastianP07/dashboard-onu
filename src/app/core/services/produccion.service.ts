import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { GlobalServicesRequests } from './global.service';

@Injectable()
export class ProduccionServices {
  constructor(private globalServicesRequests: GlobalServicesRequests) {}

  registrarProgramaAcademico(dataPrograma): Observable<any> {
    return this.globalServicesRequests.post('crearProgramaAcademico/', dataPrograma);
  }
  
  registrarDiplomado(dataPrograma): Observable<any> {
    return this.globalServicesRequests.post('crearDiplomado/', dataPrograma);
  }
  
  registrarCursoProgramaAcademico(dataPrograma): Observable<any> {
    return this.globalServicesRequests.post('crearCursoProgramaAcademico/', dataPrograma);
  }

  verProduccionAcademicaAlmacenada(): Observable<any> {
    return this.globalServicesRequests.get('verProduccionAcademicaAlmacenada/');
  }
  
}
