import { TestBed } from '@angular/core/testing';
import {
  HttpClientTestingModule,
  HttpTestingController,
} from '@angular/common/http/testing';
import { HttpClient } from '@angular/common/http';

import { UserServices } from './user.service';
import { environment } from 'src/environments/environment';
import { GlobalServicesRequests } from './global.service';

describe('UserServices', () => {
  let httpClient: HttpClient;
  let httpTestingController: HttpTestingController;
  let service: GlobalServicesRequests;

  beforeEach(() => {
    TestBed.configureTestingModule({ imports: [HttpClientTestingModule] });

    httpClient = TestBed.inject(HttpClient);
    httpTestingController = TestBed.inject(HttpTestingController);
    service = TestBed.inject(GlobalServicesRequests);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('Test for obtenerProgramasAcademicos()', () => {
    it('should return Programas Academicos', () => {
      const expectData = {
        codigo: 1,
        descripcion: 'Respuesta OK',
        descripcionError: 'Respuesta OK',
        objetoRespuestaArrayProgramaAcademico: [
          {
            idProgramaAcademico: 1,
            nombre: 'Ingenieria de sistemas',
          },
          {
            idProgramaAcademico: 2,
            nombre: 'Ingenieria ambiental',
          },
        ],
      };

      let dataError: any;
      let dataResponse: any;

      service.get('verProgramasAcademicos/').subscribe(
        (response) => {
          dataResponse = response;
        },
        (error) => {
          dataError = error;
        }
      );

      const req = httpTestingController.expectOne(
        `${environment.url}verProgramasAcademicos/`
      );
      req.flush(expectData);

      expect(req.request.method).toEqual('GET');
      expect(dataError).toBeUndefined();
    });
  });
});
