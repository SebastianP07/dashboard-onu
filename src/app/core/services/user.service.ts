import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { GlobalServicesRequests } from './global.service';

@Injectable()
export class UserServices {
  constructor(private globalServicesRequests: GlobalServicesRequests) {}

  registrarUsuario(dataUsuario): Observable<any> {
    return this.globalServicesRequests.post('registrarUsuario/', dataUsuario);
  }

  iniciarSesion(dataUsuario): Observable<any> {
    return this.globalServicesRequests.post('iniciarSesion/', dataUsuario);
  }

  obtenerDataGlobal(): Observable<any> {
    return this.globalServicesRequests.get('obtenerDatosGlobales/');
  }
  
  obtenerProgramasAcademicos(): Observable<any> {
    return this.globalServicesRequests.get('verProgramasAcademicos/');
  }


}
