import { Injectable } from '@angular/core';
import { Endpoints, getHeadersHttpAuthorization } from '../models/endpoints';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class GlobalServicesRequests {
    private url: string;
    private headersHttp: any;

    constructor(
        private _http: HttpClient
    ) {
        this.url = Endpoints.url;
        this.headersHttp = getHeadersHttpAuthorization();
    }

    post(peticion, dataUsuario): Observable<any> {
        console.log("GlobalServicesRequests POST", this.url + peticion, dataUsuario);
        return this._http.post(this.url + peticion, dataUsuario, this.headersHttp);
    }

    get(peticion): Observable<any> {
        console.log("GlobalServicesRequests GET", this.url + peticion);
        return this._http.get(this.url + peticion, this.headersHttp);
    }
}

