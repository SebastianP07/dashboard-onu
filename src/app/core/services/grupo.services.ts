import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { GlobalServicesRequests } from './global.service';

@Injectable()
export class GrupoServices {
  constructor(private globalServicesRequests: GlobalServicesRequests) {}

  registrarGrupo(dataGrupo): Observable<any> {
    return this.globalServicesRequests.post('crearGrupoInvestigacion/', dataGrupo);
  }

  verGruposRegistrados(): Observable<any> {
    return this.globalServicesRequests.get('verGrupoInvestigacion/');
  }


}
