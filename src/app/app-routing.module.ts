import { NgModule } from '@angular/core';
import { Routes, RouterModule, PreloadAllModules } from '@angular/router';
import { InicioSesionGuard } from './core/guards/inicio-sesion.guard';

const routes: Routes = [
  {
    path: '',
    loadChildren: () => import('./dashboard/dashboard.module').then(m => m.DashboardModule),
    canActivate: [InicioSesionGuard]
  },
  {
    path: 'inicio-sesion',
    loadChildren: () => import('./inicio-sesion/inicio-sesion.module').then(m => m.InicioSesionModule),
  },
  {
    path: 'registro-usuario',
    loadChildren: () => import('./registro-usuario/registro-usuario.module').then(m => m.RegistroUsuarioModule),
  },
  { path: 'Ver-historial-investigador', loadChildren: () => import('./dashboard/investigadores/historial-investigador/ver-historial-investigador/ver-historial-investigador.module').then(m => m.VerHistorialInvestigadorModule) }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
