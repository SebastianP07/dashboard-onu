import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RegistroUsuarioRoutingModule } from './registro-usuario-routing.module';
import { RegistroUsuarioComponent } from './registro-usuario.component';
import { ReactiveFormsModule } from '@angular/forms';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { GlobalComponent } from '../dashboard/global-components/global-components';


@NgModule({
  declarations: [RegistroUsuarioComponent],
  imports: [
    CommonModule,
    RegistroUsuarioRoutingModule,
    ReactiveFormsModule,
    BsDatepickerModule.forRoot(),
    GlobalComponent
  ]
})
export class RegistroUsuarioModule { }
