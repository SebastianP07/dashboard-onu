import { Component, OnInit } from '@angular/core';
import {
  FormGroup,
  FormBuilder,
  FormControl,
  Validators,
} from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { BsLocaleService } from 'ngx-bootstrap/datepicker';
import { defineLocale } from 'ngx-bootstrap/chronos';
import { esLocale } from 'ngx-bootstrap/locale';
import { UserServices } from '../core/services/user.service';
import * as crypto from '../../../node_modules/crypto-js'; 
import { DatePipe } from '@angular/common';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { ModalComponent } from '../core/modal/modal.component';

@Component({
  selector: 'app-registro-usuario',
  templateUrl: './registro-usuario.component.html',
  styleUrls: ['./registro-usuario.component.css'],
  providers: [UserServices]
})
export class RegistroUsuarioComponent implements OnInit {
  bsModalRef: BsModalRef;
  userForm: FormGroup;
  datePipe = new DatePipe('en-US');

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private localeService: BsLocaleService,
    private userServices: UserServices,
    private modalService: BsModalService
  ) {
    defineLocale('es', esLocale);
    this.userForm = this.formBuilder.group({
      numeroidentificacion: new FormControl(
        // Valor por defecto
        '',
        // Conjunto de validaciones
        Validators.compose([Validators.required])
      ),
      genero: new FormControl(
        // Valor por defecto
        '',
        // Conjunto de validaciones
        Validators.compose([Validators.required])
      ),
      nombre: new FormControl(
        // Valor por defecto
        '',
        // Conjunto de validaciones
        Validators.compose([Validators.required])
      ),
      primerapellido: new FormControl(
        // Valor por defecto
        '',
        // Conjunto de validaciones
        Validators.compose([Validators.required])
      ),
      segundoapellido: new FormControl(
        // Valor por defecto
        '',
        // Conjunto de validaciones
        //Validators.compose([Validators.required])
      ),
      fechanacimiento: new FormControl(
        // Valor por defecto
        '',
        // Conjunto de validaciones
        Validators.compose([Validators.required])
      ),
      direccion: new FormControl(
        // Valor por defecto
        '',
        // Conjunto de validaciones
        Validators.compose([Validators.required])
      ),
      telefono: new FormControl(
        // Valor por defecto
        '',
        // Conjunto de validaciones
        Validators.compose([Validators.required])
      ),
      correoElectronico: new FormControl(
        // Valor por defecto
        '',
        // Conjunto de validaciones
        Validators.compose([
          Validators.required,
          Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$'),
        ])
      ),
      fechainscripcion: new FormControl(
        // Valor por defecto
        ''
      ),
      password: new FormControl(
        // Valor por defecto
        '',
        // Conjunto de validaciones
        Validators.compose([Validators.required, Validators.minLength(5)])
      ),
    });
  }

  ngOnInit(): void {}

  submitFormuser(dataUser) {
    dataUser.password = crypto.SHA512(dataUser.password).toString();
    dataUser.fechainscripcion = this.datePipe.transform(new Date(), 'dd/MM/yyyy');
    dataUser.fechanacimiento = this.datePipe.transform(dataUser.fechanacimiento, 'dd/MM/yyyy');
    
    this.userServices.registrarUsuario(dataUser).subscribe(
      (response) => {
        console.log('response submitFormuser => ', response);
        if(response.codigo == 1){
          this.userForm.reset();
          this.openModalWithComponent('Registro con éxito.', response.descripcion);
        } else {
          this.userForm.reset();
          this.openModalWithComponent('Error al registrar.', response.descripcion);
          console.error("Error servicio submitFormuser =>", response);
        }
      },
      (error) => {
        this.userForm.reset();
        console.error('error submitFormuser => ', error);
        this.openModalWithComponent('Error del servicio.', 'Error al registrar el usuario, por favor contacte con sorporte.');
      }
    );
  }

  goToComponent(componente) {
    this.router.navigate([componente]);
  }

  openModalWithComponent(titulo, mensaje) {
    const initialState = {
      title: `${titulo}`,
      list: `${mensaje}`,
    };
    this.bsModalRef = this.modalService.show(ModalComponent, { initialState });
    this.bsModalRef.content.closeBtnName = 'Cerrar';
  }
}
