import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, NavigationStart, NavigationEnd } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'dashboard-onu';
  datosUsuario: any;
  sesionActiva: boolean;

  constructor(
    private router: Router,
    private activatedRoute: ActivatedRoute
  ) {
  }

  ngOnInit() {
    /*
    this.router.events.subscribe(event => {
      const datosUsuario = localStorage.getItem('datos-usuario');
      if (event instanceof NavigationStart) {
        this.sesionActiva = ((datosUsuario === null) ? false : true);
      }
    }, error => {
      console.error(error)
    });
    */
  }
}