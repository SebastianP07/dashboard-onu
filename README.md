# Proyecto Dashboard ONU - Universidad Central 2020

- [Angular CLI](https://github.com/angular/angular-cli) version 9.1.0.

- URL de despliegue: https://sebastianp07.gitlab.io/dashboard-onu

## Archivo configuracion despliegue Gitlab Pages

```yml
image: node:latest
stages:
  - install
  - test
  - build
  - deploy

install-dependecies:
  stage: install
  script:
    - npm install
  artifacts:
    expire_in: 1hr
    paths:
      - node_modules/
  cache:
    paths:
      - node_modules/
  
test-apps:
  stage: test
  variables:
    CHROME_BIN: google-chrome
    dependencies: install-dependencies
  before_script:
    - apt-get update && apt-get install -y apt-transport-https
    - wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | apt-key add -
    - sh -c 'echo "deb http://dl.google.com/linux/chrome/deb/ stable main" >> /etc/apt/sources.list.d/google.list'
    - apt-get update && apt-get install -y google-chrome-stable
  script:
    - npm run test:ci

build-app:
  stage: build
  variables:
    BUILD_CONFIGURATION: 'production'
  dependencies: 
    - install-dependecies
  script:
    - npm run build
  artifacts:
    expire_in: 1hr
    paths:
      - dist/

pages:
  stage: deploy
  dependencies: 
    - build-app
  script:
    - mkdir public
    - mv ./dist/dashboard-onu/* ./public/
  artifacts:
    paths:
      - public
  environment:
    name: production
  only:
    - master
```
# Screenshots
![image](/src/assets/imagenes/screenshots/Dashboard_screenshots_1.png)
![image](/src/assets/imagenes/screenshots/Dashboard_screenshots_2.png)

## Autor ✒️

* **Sebastian Pinto Becerra** - *Trabajo Inicial* - [SebastianP07](https://gitlab.com/SebastianP07)
* **Sebastian Pinto Becerra** - *Documentación* -  [SebastianP07](https://gitlab.com/SebastianP07)

También puedes mirar la lista de todos los [contribuyentes](https://gitlab.com/SebastianP07/dashboard-onu/-/graphs/master) quíenes han participado en este proyecto. 


---
⌨️ por [SebastianP07](https://github.com/SebastianP07) ☕
